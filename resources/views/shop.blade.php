@extends('layouts.front')

@section('content')
    <div class="ht__bradcaump__area bg-image--68">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Shop Grid</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="/">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">Shop</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="page-shop-sidebar left--sidebar bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-12 order-2 order-lg-1 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar">
                        <aside class="wedget__categories poroduct--cat">
                            <h3 class="wedget__title">Product Categories</h3>
                            <ul>
                                <li><a href="#">Accessories <span>(3)</span></a></li>
                                <li><a href="#">Bedroom <span>(4)</span></a></li>
                                <li><a href="#">clothings <span>(6)</span></a></li>
                                <li><a href="#">dining room <span>(7)</span></a></li>
                                <li><a href="#">Hand Bags <span>(8)</span></a></li>
                                <li><a href="#">HeadPhone <span>(9)</span></a></li>
                                <li><a href="#">jeans <span>(13)</span></a></li>
                                <li><a href="#">Living Room <span>(20)</span></a></li>
                                <li><a href="#">Men <span>(22)</span></a></li>
                                <li><a href="#">Shoes <span>(13)</span></a></li>
                                <li><a href="#">Speaker <span>(17)</span></a></li>
                                <li><a href="#">Suits <span>(20)</span></a></li>
                                <li><a href="#">Television <span>(34)</span></a></li>
                                <li><a href="#">Accessories <span>(60)</span></a></li>
                                <li><a href="#">Women <span>(3)</span></a></li>
                                <li><a href="#">Suits <span>(3)</span></a></li>
                            </ul>
                        </aside>

{{--                        <aside class="wedget__categories pro--range">--}}
{{--                            <h3 class="wedget__title">Filter by price</h3>--}}
{{--                            <div class="content-shopby">--}}
{{--                                <div class="price_filter s-filter clear">--}}
{{--                                    <form action="#" method="GET">--}}
{{--                                        <div id="slider-range"></div>--}}
{{--                                        <div class="slider__range--output">--}}
{{--                                            <div class="price__output--wrap">--}}
{{--                                                <div class="price--output">--}}
{{--                                                    <span>Price :</span><input type="text" id="amount" readonly>--}}
{{--                                                </div>--}}
{{--                                                <div class="price--filter">--}}
{{--                                                    <a href="#">Filter</a>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </form>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </aside>--}}

{{--                        <aside class="wedget__categories poroduct--compare">--}}
{{--                            <h3 class="wedget__title">Compare</h3>--}}
{{--                            <ul>--}}
{{--                                <li><a href="#">x</a><a href="#">Condimentum posuere</a></li>--}}
{{--                                <li><a href="#">x</a><a href="#">Condimentum posuere</a></li>--}}
{{--                                <li><a href="#">x</a><a href="#">Dignissim venenatis</a></li>--}}
{{--                            </ul>--}}
{{--                        </aside>--}}

{{--                        <aside class="wedget__categories poroduct--tag">--}}
{{--                            <h3 class="wedget__title">Product Tags</h3>--}}
{{--                            <ul>--}}
{{--                                <li><a href="#">blouse</a></li>--}}
{{--                                <li><a href="#">clothes</a></li>--}}
{{--                                <li><a href="#">fashion</a></li>--}}
{{--                                <li><a href="#">handbag</a></li>--}}
{{--                                <li><a href="#">fashion</a></li>--}}
{{--                                <li><a href="#">handbag</a></li>--}}
{{--                                <li><a href="#">handbag</a></li>--}}
{{--                                <li><a href="#">fashion</a></li>--}}
{{--                                <li><a href="#">handbag</a></li>--}}
{{--                                <li><a href="#">hat</a></li>--}}
{{--                                <li><a href="#">laptop</a></li>--}}
{{--                                <li><a href="#">hat</a></li>--}}
{{--                                <li><a href="#">laptop</a></li>--}}
{{--                            </ul>--}}
{{--                        </aside>--}}

                        <aside class="wedget__categories sidebar--banner">
                            <img src="images/others/banner_left.jpg" alt="banner images">
                            <div class="text">
                                <h2>new products</h2>
                                <h6>save up to <br> <strong>40%</strong>off</h6>
                            </div>
                        </aside>


                    </div>
                </div>
                <div class="col-lg-9 col-12 order-1 order-lg-2">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="shop__list__wrapper d-flex flex-wrap flex-md-nowrap justify-content-between">
                                <div class="shop__list nav justify-content-center" role="tablist">
                                    <a class="nav-item nav-link active" data-toggle="tab" href="#nav-grid" role="tab"><i class="fa fa-th"></i></a>
{{--                                    <a class="nav-item nav-link" data-toggle="tab" href="#nav-list" role="tab"><i class="fa fa-list"></i></a>--}}
                                </div>
{{--                                <p>Showing 1–12 of 40 results</p>--}}
{{--                                <div class="orderby__wrapper">--}}
{{--                                    <span>Sort By</span>--}}
{{--                                    <select class="shot__byselect">--}}
{{--                                        <option>Default sorting</option>--}}
{{--                                        <option>HeadPhone</option>--}}
{{--                                        <option>Furniture</option>--}}
{{--                                        <option>Jewellery</option>--}}
{{--                                        <option>Handmade</option>--}}
{{--                                        <option>Kids</option>--}}
{{--                                    </select>--}}
{{--                                </div>--}}
                            </div>
                        </div>
                    </div>
                    <div class="tab__container">
                        <div class="shop-grid tab-pane fade show active" id="nav-grid" role="tabpanel">
                            <div class="row">
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/1.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
{{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/3.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">New</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Voyage Yoga Bag</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/4.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Impulse Duffle</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/5.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/2.jpg" alt="product image"></a>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$60.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Beginner's Yoga</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/6.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/5.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/product/7.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/3.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/elec-product/1.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/elec-product/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">new</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/elec-product/3.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/elec-product/4.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">New</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$70.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Impulse Duffle</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/flower/4.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/flower/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/furniture/md-2/1.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/furniture/md-2/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">new</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/furniture/md-2/3.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/furniture/md-2/4.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">New</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$70.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Impulse Duffle</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="col-lg-4 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product')}}"><img src="images/handmade/1.jpg" alt="product image"></a>
                                            <a class="second__img animation1" href="{{route('product')}}"><img src="images/handmade/2.jpg" alt="product image"></a>
                                            <div class="new__box">
                                                <span class="new-label">Hot</span>
                                            </div>
                                            <ul class="prize position__right__bottom d-flex">
                                                <li>$40.00</li>
                                                <li class="old_prize">$55.00</li>
                                            </ul>
                                            <div class="action">
                                                <div class="actions_inner">
                                                    <ul class="add_to_links">
                                                        {{--                                                        <li><a class="cart" href="cart.html"></a></li>--}}
{{--                                                        <li><a class="wishlist" href="wishlist.html"></a></li>--}}
{{--                                                        <li><a class="compare" href="compare.html"></a></li>--}}
                                                        <li><a data-toggle="modal" title="Quick View" class="quickview modal-view detail-link" href="#productmodal"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product')}}">Strive Shoulder Pack</a></h4>
                                            <ul class="rating d-flex">
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li class="on"><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                                <li><i class="fa fa-star-o"></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!-- End Single Product -->
                            </div>
                            <ul class="wn__pagination">
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#"><i class="zmdi zmdi-chevron-right"></i></a></li>
                            </ul>
                        </div>
                        <div class="shop-grid tab-pane fade" id="nav-list" role="tabpanel">
                            <div class="list__view__wrapper">
                                <!-- Start Single Product -->
                                <div class="list__view">
                                    <div class="thumb">
                                        <a class="first__img" href="{{route('product')}}"><img src="images/product/1.jpg" alt="product images"></a>
                                        <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/2.jpg" alt="product images"></a>
                                    </div>
                                    <div class="content">
                                        <h2><a href="{{route('product')}}">adipiscing</a></h2>
                                        <ul class="rating d-flex">
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="prize__box">
                                            <li>£111.00</li>
                                            <li class="old__prize">£220.00</li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                        <ul class="cart__action d-flex">
                                            <li class="cart"><a href="cart.html">Add to cart</a></li>
                                            <li class="wishlist"><a href="cart.html"></a></li>
                                            <li class="compare"><a href="cart.html"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="list__view mt--40">
                                    <div class="thumb">
                                        <a class="first__img" href="{{route('product')}}"><img src="images/product/3.jpg" alt="product images"></a>
                                        <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/4.jpg" alt="product images"></a>
                                    </div>
                                    <div class="content">
                                        <h2><a href="{{route('product')}}">Voyage Yoga Bag</a></h2>
                                        <ul class="rating d-flex">
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="prize__box">
                                            <li>£111.00</li>
                                            <li class="old__prize">£220.00</li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                        <ul class="cart__action d-flex">
                                            <li class="cart"><a href="cart.html">Add to cart</a></li>
                                            <li class="wishlist"><a href="cart.html"></a></li>
                                            <li class="compare"><a href="cart.html"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="list__view mt--40">
                                    <div class="thumb">
                                        <a class="first__img" href="{{route('product')}}"><img src="images/product/5.jpg" alt="product images"></a>
                                        <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/6.jpg" alt="product images"></a>
                                    </div>
                                    <div class="content">
                                        <h2><a href="{{route('product')}}">Impulse Duffle</a></h2>
                                        <ul class="rating d-flex">
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="prize__box">
                                            <li>£111.00</li>
                                            <li class="old__prize">£220.00</li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                        <ul class="cart__action d-flex">
                                            <li class="cart"><a href="cart.html">Add to cart</a></li>
                                            <li class="wishlist"><a href="cart.html"></a></li>
                                            <li class="compare"><a href="cart.html"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="list__view mt--40">
                                    <div class="thumb">
                                        <a class="first__img" href="{{route('product')}}"><img src="images/product/7.jpg" alt="product images"></a>
                                        <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/6.jpg" alt="product images"></a>
                                    </div>
                                    <div class="content">
                                        <h2><a href="{{route('product')}}">Strive Shoulder Pack</a></h2>
                                        <ul class="rating d-flex">
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="prize__box">
                                            <li>£111.00</li>
                                            <li class="old__prize">£220.00</li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                        <ul class="cart__action d-flex">
                                            <li class="cart"><a href="cart.html">Add to cart</a></li>
                                            <li class="wishlist"><a href="cart.html"></a></li>
                                            <li class="compare"><a href="cart.html"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- End Single Product -->
                                <!-- Start Single Product -->
                                <div class="list__view mt--40">
                                    <div class="thumb">
                                        <a class="first__img" href="{{route('product')}}"><img src="images/product/8.jpg" alt="product images"></a>
                                        <a class="second__img animation1" href="{{route('product')}}"><img src="images/product/9.jpg" alt="product images"></a>
                                    </div>
                                    <div class="content">
                                        <h2><a href="{{route('product')}}">Strive Shoulder Pack</a></h2>
                                        <ul class="rating d-flex">
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li class="on"><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                            <li><i class="fa fa-star-o"></i></li>
                                        </ul>
                                        <ul class="prize__box">
                                            <li>£111.00</li>
                                            <li class="old__prize">£220.00</li>
                                        </ul>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec non est at libero vulputate rutrum. Morbi ornare lectus quis justo gravida semper. Nulla tellus mi, vulputate adipiscing cursus eu, suscipit id nulla.</p>
                                        <ul class="cart__action d-flex">
                                            <li class="cart"><a href="cart.html">Add to cart</a></li>
                                            <li class="wishlist"><a href="cart.html"></a></li>
                                            <li class="compare"><a href="cart.html"></a></li>
                                        </ul>

                                    </div>
                                </div>
                                <!-- End Single Product -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')

@endpush
@push('scripts')

@endpush
