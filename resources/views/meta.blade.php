<meta name="description"
      content="Solutions For SMS Gateways - SMS API Integrations. Send SMS by your business name - Unique Sender IDs for your business,Marketing Bulk SMS service for your business and many other software solutions .">

<!-- Open Graph / Facebook -->
<meta property="og:type" content="website">
<meta property="og:url" content="https://kentacy.com/">
<meta property="og:title" content="Kentacy — Best SMS Gateway Provider In Sri Lanka">
<meta property="og:description"
      content="Solutions For SMS Gateways - SMS API Integrations. Send SMS by your business name - Unique Sender IDs for your business,Marketing Bulk SMS service for your business and many other software solutions .">
<meta property="og:image" content="{{asset('media/misc/logo.png')}}">


<!-- Twitter -->
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://kentacy.com/">
<meta property="twitter:title" content="Kentacy — Best SMS Gateway Provider In Sri Lanka">
<meta property="twitter:description"
      content="Solutions For SMS Gateways - SMS API Integrations. Send SMS by your business name - Unique Sender IDs for your business,Marketing Bulk SMS service for your business and many other software solutions .">
<meta property="twitter:image" content="{{asset('media/misc/logo.png')}}">
<link rel="icon" href="{{asset('media/misc/icon.png')}}">
