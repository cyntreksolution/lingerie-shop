<?php

namespace App\Http\Controllers;

use App\Events\BulkCampaignCreated;
use App\Models\Business;
use App\Models\Campaign;
use App\Models\CampaignContact;
use App\Models\CampaignNumber;
use App\Models\Contact;
use App\Models\SenderID;
use App\Traits\SMSPriceTrait;
use App\Traits\SMSTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class APIController extends Controller
{
    use SMSTrait;
    use SMSPriceTrait;

    public function sendInstantMessage(Request $request)
    {
        $rules = array(
            'business_id' => 'required',
            'business_secret' => 'required',
            'sender_id' => 'required',
            'message' => 'required',
            'number' => 'required|digits:10',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->sendError($errors, 'Required Fields Missing !');
        }


        $business_id = $request->business_id;
        $business_secret = $request->business_secret;
        $sender_id = $request->sender_id;
        $number = $request->number;
        $message = $request->message;

        $business = Business::whereBusinessId($business_id)->whereBusinessSecret($business_secret)->first();

        if (empty($business) || $business->count() == 0) {
            return $this->sendError('Authentication Failed!');
        }

        $business_sender_id = SenderID::whereSenderId($sender_id)->whereStatus('active')->first();

        if (empty($business_sender_id) || $business_sender_id->count() == 0 || $business_sender_id->business_id != $business->id) {
            return $this->sendError('Invalid Sender ID !');
        }

        $campaign = new Campaign();
        $campaign->business_id = $business->id;
        $campaign->user_id = Auth::id();
        $campaign->sender_id = $business_sender_id->id;
        $campaign->type = 'individual_api';
        $campaign->message = $message;
//        $campaign->is_scheduled = !empty($is_schedule) ? 1 : 0;
//        $campaign->scheduled_time = !empty($is_schedule) ? Carbon::parse($schedule_time)->format('Y-m-d H:i') : null;
        $campaign->status = 'sent';
        $campaign->sms_count = 0;
        $campaign->page_count = $this->count($message)->messages;
        $campaign->cost_per_sms = $business->sms_charge;
        $campaign->start_balance = 0;
        $campaign->sms_charge = 0;
        $campaign->end_balance = 0;

        $campaign->save();

        if (!empty($number)) {
            $cn = CampaignNumber::firstOrCreate(['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $number]);
        }


        $number_count = CampaignNumber::whereCampaignId($campaign->id)->count();
        $campaign->sms_count = $number_count;

        $charge = ($campaign->sms_count * $campaign->page_count) * $campaign->cost_per_sms;

        $real_cost =  ($campaign->sms_count * $campaign->page_count) * 0.62;
        $profit = $charge -$real_cost;

        $campaign->profit = $profit;
        $campaign->sms_charge = $charge;

        $campaign->save();

        if ($this->hasActiveSubscription($business)) {
            $created = BulkCampaignCreated::dispatch($campaign);
        }elseif ($campaign->sms_charge < $business->balance){
            $charge = ($campaign->sms_count * $campaign->page_count) * $campaign->cost_per_sms;
            $real_cost = ($campaign->sms_count * $campaign->page_count) * 0.62;
            $profit = $charge - $real_cost;

            $campaign->profit = $profit;
            $campaign->sms_charge = $charge;
            $campaign->save();
            $created = BulkCampaignCreated::dispatch($campaign);
        }else{
            $campaign->status = 'failed';
            $campaign->error_code = 'insufficient_balance';
            $campaign->save();
            return $this->sendError('Your Account Balance Is Insufficient . Please Top UP');
        }
        return $this->sendResponse($campaign, 'Message Sent Successfully');

    }

    public function checkAccountBalance(Request $request)
    {
        $rules = array(
            'business_id' => 'required',
            'business_secret' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            return $this->sendError($errors, 'Required Fields Missing !');
        }


        $business_id = $request->business_id;
        $business_secret = $request->business_secret;

        $business = Business::whereBusinessId($business_id)->whereBusinessSecret($business_secret)->first();

        if (empty($business) || $business->count() == 0) {
            return $this->sendError('Authentication Failed!');
        }

        $data = [
            'business_name' => $business->name,
            'account_balance' => number_format($business->balance, 2)
        ];
        return $this->sendResponse($data, 'Business Account Balance');

    }


    public function deliveryStatus(Request $request){
        Storage::disk('local')->put('file.txt', 'Your content here');
    }
}
