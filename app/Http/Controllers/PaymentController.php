<?php

namespace App\Http\Controllers;

use App\Models\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Http;
use Ixudra\Curl\Facades\Curl;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('payments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  dd($request->all());
        $_type = "ONE_TIME";
        $_mId = "IC02088";
        $_amount = $request->amount;
        $_firstName = "Ishara";
        $_lastName = "Sewwandi";
        $_email = "yaisharasewwandi@gmail.com";
        $_reference = "DP12345";
        $_description = "test Product";
        $_returnUrl = "http://127.0.0.1:8000/payments";
        $_cancelUrl = "http://127.0.0.1:8000/topup";
        $_responseUrl = "http://127.0.0.1:8000/payments";
        $_currency = "LKR";
        $_orderId = "DP12355";
        $_pluginVersion = "1.0";
        $_pluginName = "CUSTOM";
        $api_key = "7aa777f1671eb78e56395e7bf275c6e9012faee3fac8dcb34b2f549425b2982e";
        $_startDate = "2020-11-20";
        $_endDate = "2020-11-20";
        $_interval = "MONTHLY";
        $_doFirstPayment = "yes";

        $localUrl = "https://testpay.directpay.lk/";

        $keyfile = '-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBALUWOzPr24Y7CpMy0yQ/JwwfyMIItevUzKx+ovhMmyq3y6Eu2DSJ
5XHjSgRqaPWJL4CUThCy0BwbwQmE6vheJ4UCAwEAAQJAEISYneHTwmi8TDUEoXfJ
PgfgLiLRRVPB37Ild3S1aq31t5SedMBI0EIzOgQOB0va/r393cBIUxZ7pmScl4ni
wQIhAPAgrdKyS4hwsRf6AfGF3LqrJhjIBlpWbmZvOzuQggmJAiEAwQ5+MY75Lcoq
xCVVBc9YxDQHUdDCz46+G/T6KYhMux0CIQCTmcK+7FF5gKuarVZce4f+Rg2Y1frx
tIcP9/dU2bLU4QIhAJaa7ctPlAgqrM4zqW5M4Ry+0e77gt5Upu48YGPCFsnpAiAW
/1ViaalSdfdz20AATRRLGe2zM3QLcCz72hbFvZyoWw==
-----END RSA PRIVATE KEY-----';
        $pri_key = openssl_pkey_get_private($keyfile);
        $merchant = $_mId;
        $pluginName = $_pluginName;
        $pluginVersion = $_pluginVersion;
        $returnUrl = $_returnUrl;
        $cancelUrl = $_cancelUrl;
        $orderId = $_orderId;
        $reference = $_reference;
        $firstName = $_firstName;
        $lastName = $_lastName;
        $email = $_email;
        $description = $_description;
        $apiKey = $api_key;
        $responseUrl = $_responseUrl;
        $type = $_type;
        $amount = $_amount;
        $currency = $_currency;
        $startDate = '';
        $endDate = '';
        $interval = '';
        $doFirstPayment = $_doFirstPayment;




        $priKey = '-----BEGIN RSA PRIVATE KEY-----
MIIBOwIBAAJBALUWOzPr24Y7CpMy0yQ/JwwfyMIItevUzKx+ovhMmyq3y6Eu2DSJ
5XHjSgRqaPWJL4CUThCy0BwbwQmE6vheJ4UCAwEAAQJAEISYneHTwmi8TDUEoXfJ
PgfgLiLRRVPB37Ild3S1aq31t5SedMBI0EIzOgQOB0va/r393cBIUxZ7pmScl4ni
wQIhAPAgrdKyS4hwsRf6AfGF3LqrJhjIBlpWbmZvOzuQggmJAiEAwQ5+MY75Lcoq
xCVVBc9YxDQHUdDCz46+G/T6KYhMux0CIQCTmcK+7FF5gKuarVZce4f+Rg2Y1frx
tIcP9/dU2bLU4QIhAJaa7ctPlAgqrM4zqW5M4Ry+0e77gt5Upu48YGPCFsnpAiAW
/1ViaalSdfdz20AATRRLGe2zM3QLcCz72hbFvZyoWw==
-----END RSA PRIVATE KEY-----';
        $amount = $request->amount;
        $cardId = 5123450000000008;
        $currency = "LKR";
        $merchantId = "IC02088";
        $reference = "DP12345";
        $type = "VOID";
        //Concatenate all parameters field values into single string.
        $dataString = $amount.$cardId.$currency.$merchantId.$reference.$type;
        $signature = null;
        //Read the private key
        $pkeyid = openssl_get_privatekey($priKey);
        //Generate signature

        //    $signature  = Crypt::encryptString($dataString, $priKey, OPENSSL_ALGO_SHA256);
        $signResult = openssl_sign($dataString, $signature, $priKey, OPENSSL_ALGO_SHA256);
        //Base64 encode the signature
        $signature = base64_encode($signature);
//dd($signature);
        //Free the key from memory



        //key order must be same order generate signature dataString
        $requestBody = [
            "merchantId" => "IC02088",
            "type" => "VOID",
            "transactionId" => "1234",
            "note" => "transaction reversal."
        ];


        //   dd($signature);
        $tt = json_encode(array($requestBody));
        $response = Http::withHeaders([
            "Signature:" => $signature,
            "x-api-key:" => "7aa777f1671eb78e56395e7bf275c6e9012faee3fac8dcb34b2f549425b2982e",
            "Content-Type:" => "application/json"
        ])->post('https://dev.directpay.lk/v1/mpg/api/external/transaction/voidPayment', [
            'ETURNTRANSFER' => 'true',
            'ENCODING' => "",
            'MAXREDIRS' => 10,
            'FOLLOWLOCATION' => 'true',
            'HTTP_VERSION' => 'VERSION_1_1',

            "merchantId" => "IC02088",
            "type" => "VOID",
            "transactionId" => "1234",
            "note" => "transaction reversal."

        ]);
        echo $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Payment $payment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
