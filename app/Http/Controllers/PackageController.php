<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\BusinessPackageSubscription;
use App\Models\Contact;
use App\Models\Package;
use App\Traits\InvoiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PackageController extends Controller
{
    use InvoiceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('packages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment_gateway_charge = (($request->price / 100) * 3.5) + 0.35;
        $profit = (($request->price - $payment_gateway_charge) / $request->sms_qty) - $request->kentacy_sms_cost;

        if ($profit > 0) {
            $package = new Package();
            $package->name = $request->name;
            $package->description = $request->description;
            $package->price = $request->price;
            $package->sms_qty = $request->sms_qty;
            $package->kentacy_sms_cost = $request->kentacy_sms_cost;
            $package->status = isset($request->status) ? 1 : 0;
            $package->validity_period = $request->validity_period;
            $package->created_by = Auth::id();

            $package->user_sms_cost = $request->price / $request->sms_qty;
            $package->profit_per_sms = $profit;


            $package->save();

            return $this->sendResponse($package, 'Package Created Successfully');
        } else {
            return $this->sendError('No Profit');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Package $package
     * @return \Illuminate\Http\Response
     */
    public function show(Package $package)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Package $package
     * @return \Illuminate\Http\Response
     */
    public function edit(Package $package)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Package $package
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Package $package)
    {
        $payment_gateway_charge = (($request->price / 100) * 3.5) + 0.35;
        $profit = (($request->price - $payment_gateway_charge) / $request->sms_qty) - $request->kentacy_sms_cost;


        if ($profit > 0) {
            $package->name = $request->name;
            $package->description = $request->description;
            $package->price = $request->price;
            $package->sms_qty = $request->sms_qty;
            $package->kentacy_sms_cost = $request->kentacy_sms_cost;
            $package->status = isset($request->status) ? 1 : 0;
            $package->validity_period = $request->validity_period;
            $package->updated_by = Auth::id();

            $package->user_sms_cost = $request->price / $request->sms_qty;
            $package->profit_per_sms = $profit;
            $package->save();

            return $this->sendResponse($package, 'Package Updated Successfully');
        } else {
            return $this->sendError('No Profit');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Package $package
     * @return \Illuminate\Http\Response
     */
    public function destroy(Package $package)
    {
        $package->deleted_by = Auth::id();
        $package->save();
        $package->delete();
        return $this->sendResponse($package, 'Package Deleted Successfully');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        if (Auth::user()->hasAnyRole(['Super Admin', 'Admin'])) {
            $columns = ['id', 'name', 'description', 'price', 'sms_qty', 'validity_period', 'status', 'created_by', 'user_sms_cost', 'kentacy_sms_cost', 'profit_per_sms'];
        } else {
            $columns = ['id', 'name', 'description', 'price', 'sms_qty', 'validity_period'];
        }

        $order_column = $columns[$order_by[0]['column']];
        $dataset = Package::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Package::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('packages edit')) ? 1 : 0;
        $can_delete = ($user->can('packages destroy')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-description='{$item->description}' data-price='{$item->price}' data-sms_qty='{$item->sms_qty}' data-kentacy_sms_cost='{$item->kentacy_sms_cost}' data-status='{$item->status}' data-validity_period='{$item->validity_period}'><i class='fa fa-pencil-alt '  ></i></button>";
            }
            if ($can_delete) {
                $url = "'packages/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }

            $biz_id = Auth::user()->business->id;
            $buy_btn = "<button  class='btn btn-xs btn-light-success mr-2' onclick='buyNow($biz_id,$item->id)' data-toggle='tooltip' data-placement='top' title='Buy Now'>Buy Now</button>";
            $status = ($item->status) ? "<button  class='btn btn-xs btn-light-primary mr-2'>Enabled</button>" : "<button  class='btn btn-xs btn-light-danger mr-2'>Disabled</button>";

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    $item->name,
                    $item->description,
                    number_format($item->price, 2),
                    $item->sms_qty,
                    $item->validity_period,
                    $status,
                    $item->creator->name,
                    number_format($item->user_sms_cost, 2),
                    number_format($item->kentacy_sms_cost, 2),
                    number_format($item->profit_per_sms, 2),
                    number_format($item->profit_per_sms * $item->sms_qty, 2),
                    $buy_btn . $edit_btn . $delete_btn
                );
            } else {
                $data[$i] = array(
                    $item->id,
                    $item->name,
                    $item->description,
                    number_format($item->price, 2),
                    $item->sms_qty,
                    $item->validity_period,
                    $buy_btn
                );
            }

            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function buyPackage(Request $request)
    {
        $business = Business::find($request->business_id);
        $package = Package::find($request->package_id);

        if ($package->price <= $business->balance) {
            $subscription = [];
            try {
                DB::transaction(function ()  use ($business,$package){
                    $invoice = $this->generateInvoice($business->id, 'App/Package', $package->id, $package->price, 'paid');

                    $business->balance = $business->balance - $package->price;
                    $business->save();

                    $subscription = new BusinessPackageSubscription();
                    $subscription->business_id = $business->id;
                    $subscription->package_id = $package->id;
                    $subscription->invoice_id = $invoice->id;
                    $subscription->purchased_by = Auth::id();
                    $subscription->price = $package->price;
                    $subscription->sms_qty = $package->sms_qty;
                    $subscription->available_sms_qty = $package->sms_qty;
                    $subscription->status = 'active';
                    $subscription->start_date = Carbon::now()->format('Y-m-d H:i:s');
                    $subscription->end_date = Carbon::now()->addDays($package->validity_period)->endOfDay()->format('Y-m-d H:i:s');
                    $subscription->save();

                });
                return $this->sendResponse($subscription, 'Package Purchased Successfully ');
            } catch (\Exception $exception) {
                return $this->sendError('Something Went Wrong Please Contact System Administrator');
            }

        } else {
            return $this->sendError('Insufficient Balance To Purchase This Package');
        }

    }
}
