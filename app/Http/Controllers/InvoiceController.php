<?php

namespace App\Http\Controllers;

use App\Models\Affiliate;
use App\Models\Business;
use App\Models\Invoice;
use App\Models\SenderID;
use App\Models\TopUp;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('invoices.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $user = User::where('business_id', $invoice->business_id)->first();
        return view('invoices.invoice', compact('invoice', 'user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Invoice $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function datatable(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at', 'invoice_no', 'reference', 'status', 'amount'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Invoice::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Invoice::count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $user = Auth::user();

        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            $btnActivate = null;

            $status = $this->generateStatusLabel($item->status);
            $invoice_url = route('invoices.show', [$item->id]);

//            $upload_documents = ($item->status == 'pending') ? "<button  onclick=\"uploadDocuments(this)\" data-id='{$item->id}'  class='btn btn-xs  btn-light-warning mr-2'> <i class='fa fa-upload'></i>Upload Documents</button>" : null;
            $make_payment = ($item->status == 'pending') ? " <button onclick=\"paymentInvoiceId(this)\" data-id='{$item->id}'  data-invoice ='{$item}'  data-user='{$item->business->owner}' class='btn btn-xs  btn-danger mr-2' > <i class='fa fa-dollar-sign'></i>Payment</button>" : null;
            $invoice = "<a  target='_blank'  href='$invoice_url' class='btn btn-xs btn-light-success mr-2' data-toggle='tooltip' data-placement='top' title='View Invoice'> <i class='fa fa-file-invoice-dollar'></i>View Invoice</a>";

            if ($user->can('invoice activate')) {
                if ($item->status == 'pending' || $item->status == 'approving') {
                    $btnActivate = "<button  onclick=\"activateInvoiceID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Pay Now</button>";
                }
            }

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                    optional($item->business)->name,
                    $item->invoice_no,
                    $item->description,
                    $status,
                    number_format($item->amount, 2) . ' LKR',
                    $make_payment . $btnActivate . $invoice
                );
            } else {
                $data[$i] = [
                    $item->id,
                    Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                    $item->invoice_no,
                    $item->description,
                    $status,
                    number_format($item->amount, 2) . ' LKR',
                    $make_payment . $invoice
                ];
            }
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function generateStatusLabel($status)
    {
        $label = null;
        $text = ucfirst(str_replace('_', ' ', $status));
        switch ($status) {
            case 'pending':
                $label = "<label class='label font-weight-bold label-lg  label-warning label-inline'>$text</label>";
                break;
            case 'free':
                $label = "<label class='label font-weight-bold label-lg  label-light-danger label-inline'>$text</label>";
                break;
            case 'paid':
                $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>$text</label>";
                break;
            case 'approving':
                $label = "<label class='label font-weight-bold label-lg  label-primary label-inline'>$text</label>";
                break;
            case 'cancelled':
                $label = "<label class='label font-weight-bold label-lg  label-danger label-inline'>$text</label>";
                break;
        }
        return $label;
    }

    public function uploadReceipt(Request $request, Invoice $invoice)
    {

        if ($invoice->reference_type == 'App/SenderID') {
            $senderID = SenderID::where('invoice_id', $invoice->id)->first();
            $status = (!empty($senderID->noc_letter_path)) ? 'approving' : 'pending_documents';

            $senderID->update([
                'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_method' => "bank_transfer",
                'status' => $status,
            ]);

            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = $file->getClientOriginalName();
                $destinationPath = storage_path() . '/sender_id/receipt/';
                $file->move($destinationPath, $fileName);

                $invoice->update([
                    'receipt_path' => '/sender_id/receipt/' . $fileName,
                    'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'paid_by' => Auth::user()->id,
                    'payment_method' => "bank_transfer",
                    'status' => "approving",
                ]);

                return $this->sendResponse($fileName, 'Document Uploaded Successfully');

            } else {
                return $this->sendError('Document Uploaded Failed');
            }
        } elseif ($invoice->reference_type == 'App/TopUp') {
            $topUp = TopUp::where('invoice_id', $invoice->id)->first();
            $topUp->update([
                'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'payment_method' => "bank_transfer",
                'status' => "approving",
            ]);
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = $file->getClientOriginalName();
                $destinationPath = storage_path() . '/topup/receipt/';
                $file->move($destinationPath, $fileName);

                $invoice->update([
                    'receipt_path' => '/topup/receipt/' . $fileName,
                    'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'paid_by' => Auth::user()->id,
                    'payment_method' => "bank_transfer",
                    'status' => "approving",
                ]);

                return $this->sendResponse($fileName, 'Document Uploaded Successfully');

            } else {
                return $this->sendError('Document Uploaded Failed');
            }
        } elseif ($invoice->reference_type == 'App/User') {
            $business = Business::find($invoice->business_id);
            $business->is_demo = 0;
            $business->save();
            if ($request->hasFile('file')) {
                $file = $request->file('file');
                $fileName = $file->getClientOriginalName();
                $destinationPath = storage_path() . '/register/receipt/';
                $file->move($destinationPath, $fileName);

                $invoice->update([
                    'receipt_path' => '/register/receipt/' . $fileName,
                    'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'paid_by' => Auth::user()->id,
                    'payment_method' => "bank_transfer",
                    'status' => "approving",
                ]);

                return $this->sendResponse($fileName, 'Document Uploaded Successfully');

            } else {
                return $this->sendError('Document Uploaded Failed');
            }
        }


    }

    public function activate(Request $request, Invoice $invoiceID)
    {

        if (Auth::user() && Auth::user()->can('invoice activate')) {
            $invoiceID = Invoice::find($request->invoice_id);
            if ($invoiceID->reference_type == 'App/SenderID') {

                $invoiceID->update([
                    'status' => 'paid',
                    'approved_by' => Auth::id(),
                    'approved_at' => Carbon::now()->format('Y-m-d H:i:s'),

                ]);

                $senderID = SenderID::where('invoice_id', $invoiceID->id)->first();
                $senderID->status = 'active';
                $senderID->verified_at = Carbon::now()->format('Y-m-d H:i:s');
                $senderID->verified_by = Auth::id();
                $senderID->save();
            } elseif ($invoiceID->reference_type == 'App/TopUp') {
                $invoiceID->update([
                    'status' => 'paid',
                    'approved_by' => Auth::id(),
                    'approved_at' => Carbon::now()->format('Y-m-d H:i:s'),

                ]);

                $topUp = TopUp::where('invoice_id', $invoiceID->id)->first();
                $topUp->status = 'paid';
                $topUp->approved_at = Carbon::now()->format('Y-m-d H:i:s');
                $topUp->approved_by = Auth::id();
                $topUp->save();

                $business = Business::where('id', $invoiceID->business_id)->first();
                $_balance = $business->balance;
                $new_balance = ($_balance + $invoiceID->amount);

                $business->update([
                    'balance' => $new_balance,

                ]);
            } elseif ($invoiceID->reference_type == 'App/User') {
                $invoiceID->status = 'paid';
                $invoiceID->approved_by = Auth::id();
                $invoiceID->approved_at = Carbon::now()->format('Y-m-d H:i:s');
                $invoiceID->save();


                $business = Business::where('id', $invoiceID->business_id)->first();
                $owner = $business->owner;

                $affiliate = Affiliate::where('affiliate_user_id', '=', $owner->id)->first();

                if (!empty($affiliate) && $affiliate->count() > 0) {
                    $affiliate_business = Business::find($affiliate->owner_business_id);

                    $current_balance = $affiliate_business->affiliate_balance;

                    $affiliate_business->affiliate_balance = $current_balance + $affiliate->amount;
                    $affiliate_business->save();

                    $affiliate->is_paid = 1;
                    $affiliate->start_balance = $current_balance;
                    $affiliate->amount = 500;
                    $affiliate->end_balance = $current_balance + $affiliate->amount;
                    $affiliate->save();
                }
            }


            // Mail::to($invoiceID->business->owner)->queue(new SenderIdActivated($invoiceID));
            return $this->sendResponse($invoiceID, 'Registration Activated Successfully!');
        } else {
            return $this->sendError('Permission Denied');
        }
    }
}
