<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Item;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $brands = Brand::pluck('name','id');
        $categories = Category::pluck('name','id');
        return view('item.index',compact('brands','categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = new Item();
        $item->category_id = $request->category;
        $item->brand_id = $request->brand;
        $item->name = $request->name;
        $item->price = $request->price;
        $item->compare_price = $request->compared_price;
        $item->description = $request->short_description;
        $item->long_description = $request->long_description;
        if ($request->file('image')){
            $file =$request->file('image');
            $destination = public_path().'/items/';
            $file_name = uniqid().'.'.$file->getClientOriginalExtension();
            $file->move($destination,$file_name);
            $item->featured_image = $file_name;
        }
        $item->save();

        $msg = 'Item Created Successfully';
        return $this->sendResponse($item, $msg);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function show(Item $item)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function edit(Item $item)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Item $product)
    {

        $product->category_id = $request->category;
        $product->brand_id = $request->brand;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->compare_price = $request->compared_price;
        $product->description = $request->short_description;
        $product->long_description = $request->long_description;
        if ($request->file('image')){
            $file =$request->file('image');
            $destination = public_path().'/items/';
            $file_name = uniqid().'.'.$file->getClientOriginalExtension();
            $file->move($destination,$file_name);
            $product->featured_image = $file_name;
        }
        $product->save();


        $msg = 'Item Updated Successfully';
        return $this->sendResponse($product, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Item  $item
     * @return \Illuminate\Http\Response
     */
    public function destroy(Item $product)
    {
        $product->delete();
        return $this->sendResponse([], 'Product Deleted');
    }


    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'name', 'description'];
        $order_column = $columns[$order_by[0]['column']];

        $query = Item::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $query = $query->get();
            $data_count = Brand::all()->count();
        } else {
            $query = $query->searchData($search)->get();
            $data_count = $query->count();
        }

        $data = [];
        $i = 0;

        $can_edit = $can_delete = 1;

        foreach ($query as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-category_id='{$item->category_id}'  data-compare_price='{$item->compare_price}'  data-price='{$item->price}' data-brand_id='{$item->brand_id}' data-name='{$item->name}' data-description='{$item->description}' data-long_description='{$item->long_description}' ><i class='fa fa-pencil-alt '  ></i></button>";
            }
            if ($can_delete) {
                $url = "'products/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }
            $src = asset('/items/'.$item->featured_image);
            $img = "<img style='width:50px ;height:50px' src='$src'>";

            $data[$i] = array(
                $item->id,
                $img,
                optional($item->category)->name,
                optional($item->brand)->name,
                $item->name,
                number_format($item->price,2).' LKR',
//                $item->description,
                $edit_btn . $delete_btn
            );


            $i++;
        }


        if ($data_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($data_count),
            "recordsFiltered" => intval($data_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
