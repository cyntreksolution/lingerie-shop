<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_user = User::firstOrCreate(['email' => 'super@cyntrek.com'],['name' => 'Super Admin', 'first_name' => 'Super', 'last_name' => 'admin', 'password' => Hash::make('super@cyntrek.com')]);
        $super_user->syncRoles(['Super Admin']);

        $super_user = User::firstOrCreate([ 'email' => 'admin@cyntrek.com'],['name' => 'System Admin', 'first_name' => 'System', 'last_name' => 'admin', 'password' => Hash::make('admin@cyntrek.com')]);
        $super_user->syncRoles(['Admin']);
    }
}
