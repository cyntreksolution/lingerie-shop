@extends('layouts.front')

@section('content')
    <div class="slider-area slider--two slide__activation slide__arrow02 owl-carousel owl-theme">
        <!-- Start Single Slide -->
        <div class="slide animation__style01 bg-image--39 slider__fixed--height align__center--left">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider__content">
                            <h2>Min</h2>
                            <h1>season scale</h1>
                            <p>Discover five of our favourite dresses from our new collection that are destined to be
                                worn and loved immediately.</p>
                            <a class="shopbtn" href="{{route('shop')}}">shopping now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Single Slide -->
        <!-- Start Single Slide -->
        <div class="slide animation__style02 bg-image--38 slider__fixed--height align__center--left">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="slider__content">
                            <h2>Passion</h2>
                            <h1>infinite creativity.</h1>
                            <p>Discover the collection as styled by fashion icon Caroline Issa in our new season
                                campaign.</p>
                            <a class="shopbtn" href="{{route('shop')}}">shopping now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Single Slide -->
    </div>

    <section class="wn__bestseller__area bg--white pt--100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section__title text-center">
                        <h2>Bestseller products</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-12 col-sm-12 mt--30">
                    <div class="product__nav nav justify-content-center" role="tablist">
                        @foreach($categories as $key=> $category)
                            <a class="nav-item nav-link {{$key==0?'active':null}}" data-toggle="tab"
                               href="#nav-{{$category->id}}" role="tab">{{$category->name}}</a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="tab__container mt--60">
                @foreach($categories as $key=> $category)
                    <div class="row single__tab tab-pane fade show {{$key==0?'active':null}}" id="nav-{{$category->id}}"
                         role="tabpanel">
                        <div class="productcategory__slide arrows_style owl-carousel owl-theme">
                            @foreach($category->products as $product)
                                <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                    <div class="product">
                                        <div class="product__thumb">
                                            <a class="first__img" href="{{route('product',[$product->id])}}"><img
                                                    src="{{asset('items/'.$product->featured_image)}}"
                                                    alt="product image"></a>
                                            <a class="second__img animation1"
                                               href="{{route('product',[$product->id])}}"><img
                                                    src="{{asset('items/'.$product->featured_image)}}"
                                                    alt="product image"></a>

                                            <div class="new__box">
                                                <span class="new-label">New</span>
                                            </div>

                                            <ul class="prize position__right__bottom d-flex">
                                                <li>{{$product->price}} LKR</li>
                                                @if(!empty($product->compare_price))
                                                    <li class="old_prize">{{$product->compare_price}} LKR</li>
                                                @endif
                                            </ul>

{{--                                            <div class="action">--}}
{{--                                                <div class="actions_inner">--}}
{{--                                                    <ul class="add_to_links">--}}
                                                        {{--                                                <li><a class="cart" href="#"></a></li>--}}
                                                        {{--                                                <li><a class="wishlist" href="#"></a></li>--}}
                                                        {{--                                                <li><a class="compare" href="#"></a></li>--}}
{{--                                                        <li><a data-toggle="modal" title="Quick View"--}}
{{--                                                               class="quickview modal-view detail-link"--}}
{{--                                                               href="#productmodal{{$product->id}}"></a></li>--}}
{{--                                                    </ul>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}

                                        </div>
                                        <div class="product__content">
                                            <h4><a href="{{route('product',[$product->id])}}">{{$product->name}}</a></h4>
{{--                                            <ul class="rating d-flex">--}}
{{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
{{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
{{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
{{--                                                <li><i class="fa fa-star-o"></i></li>--}}
{{--                                                <li><i class="fa fa-star-o"></i></li>--}}
{{--                                            </ul>--}}
                                        </div>
                                    </div>
                                </div>


                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    <section class="wn__banner__static bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <!-- Start Single Banner -->
                <div class="col-lg-4 col-sm-6 col-12">
                    <div class="hot__banner">
                        <div class="thumb">
                            <a href="#">
                                <img src="images/banner/banner-2/5.jpg" alt="banner images">
                            </a>
                        </div>
                        <div class="banner__content">
                            <h3>sale 40% <br> bag for women.</h3>
                            <p>You’ll find the piece for you in our collection of beautiful bags crafted from luxe
                                leathers, stylish shearling and super-soft suedes.</p>
                            <a class="shopbtn" href="{{route('shop')}}">shop now</a>
                        </div>
                    </div>
                </div>
                <!-- End Single Banner -->
                <!-- Start Single Banner -->
                <div class="col-lg-4 col-sm-6 col-12 xs-mt-40">
                    <div class="hot__banner box2">
                        <div class="banner__content">
                            <h3>Hot trend <br> dress for women.</h3>
                            <p>Discover five of our favourite dresses from our new collection that are destined to be
                                worn and loved immediately.</p>
                            <a class="shopbtn" href="{{route('shop')}}">shop now</a>
                        </div>
                        <div class="thumb">
                            <a href="#">
                                <img src="images/banner/banner-2/5.jpg" alt="banner images">
                            </a>
                        </div>
                    </div>
                </div>
                <!-- End Single Banner -->
                <!-- Start Single Banner -->
                <div class="col-lg-4 col-sm-6 col-12 sm-mt-40 xs-mt-40">
                    <div class="hot__banner">
                        <div class="thumb">
                            <a href="#">
                                <img src="images/banner/banner-2/4.jpg" alt="banner images">
                            </a>
                        </div>
                        <div class="banner__content">
                            <h3>spring <br> collection 2018.</h3>
                            <p>Shop transeasonal daywear, modern tailoring and opulent eveningwear for a melding of
                                contemporary shapes this season.</p>
                            <a class="shopbtn" href="{{route('shop')}}">shop now</a>
                        </div>
                    </div>
                </div>
                <!-- End Single Banner -->
            </div>
        </div>
    </section>

    <section class="wn__newsletter__area bg__cat--9">
        <div class="container">
            <div class="row newsletter--bg newsletter--bg--8 xs-mt--30 xs-pb-0">
                <div class="col-lg-7 offset-lg-5">
                    <div class="section__title text-center">
                        <h2>Sign up to newsletter</h2>
                    </div>
                    <div class="newsletter__block">
                        <p>Subscribe to our newsletters now and stay up-to-date with new collections, the latest
                            lookbooks and exclusive offers.</p>
                        <form action="#">
                            <div class="newsletter__box">
                                <input type="email" placeholder="Enter your e-mail">
                                <button>Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('styles')

@endpush
@push('scripts')

@endpush
