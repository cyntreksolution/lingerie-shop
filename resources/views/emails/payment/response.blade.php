@component('mail::message')
    Dear {{$invoice->invoiceBy->name}} ,

    Your Payment Successful...

      Invoice No : {{$invoice->invoice_no}}
          Amount : {{$invoice->amount}}.00
            Time : {{$invoice->paid_at}}
          Status : {{$invoice->status}}

@component('mail::button', ['url' => route('sender-id.index')])
View Process
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent