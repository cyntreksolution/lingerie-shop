<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable =['name','slug','description'];
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('brands.*')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }
    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('brands.id', 'like', "%" . $term . "%")
            ->orWhere('brands.name', 'like', "%" . $term . "%");
    }
}
