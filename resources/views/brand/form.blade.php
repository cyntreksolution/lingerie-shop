<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Description</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
                {!! Form::textarea('description', null, ['id'=>'description','class' => 'form-control','placeholder'=>'Enter Description','required']) !!}
    </div>
</div>



