@extends('layouts.master')
@section('title','Database Backup')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">


            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Backup List
                            <span
                                class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} ' Database Backup list</span>
                        </h3>
                    </div>

                    <div class="card-toolbar">
                        <a href="{{route('database-backup.create')}}" class="btn btn-primary">
                            New Backup
                        </a>
                    </div>

                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Created At</th>
                            <th>File Size</th>
                            <th>Downloads</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>


@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>
        DataTableOption.initDataTable('datatable', '/database-backup/data/table');
    </script>
@endpush
