<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $super_role = Role::firstOrCreate(['name' => 'Super Admin', 'guard_name' => 'web']);
        $role_admin = Role::firstOrCreate(['name' => 'Admin', 'guard_name' => 'web']);
    }
}
