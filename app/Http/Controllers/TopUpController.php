<?php

namespace App\Http\Controllers;

use App\Mail\SenderIdActivated;
use App\Models\Business;
use App\Models\Invoice;
use App\Models\TopUp;
use App\Traits\InvoiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class TopUpController extends Controller
{
    use InvoiceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('top-up.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $topUp = null;
            DB::transaction(function () use ($request) {
                $topUp = new TopUp();
                $topUp->amount = $request->amount;
                $topUp->business_id = $request->business_id;
                $topUp->created_by = Auth::user()->id;
                $topUp->balance = Auth::user()->business->balance;
                $topUp->save();

                $invoice = $this->generateInvoice($topUp->business_id, 'App/TopUp', $topUp->id, $topUp->amount, 'pending');

                $topUp->invoice_id = $invoice->id;
                $topUp->save();
            });
            return $this->sendResponse($topUp, 'Top-Up Requested Successfully');
        } catch (\Exception $exception) {
            return $this->sendError('Something went wrong Please Contact System Administrator');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\TopUp $topUp
     * @return \Illuminate\Http\Response
     */
    public function show(TopUp $topUp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\TopUp $topUp
     * @return \Illuminate\Http\Response
     */
    public function edit(TopUp $topUp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\TopUp $topUp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TopUp $topUp)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\TopUp $topUp
     * @return \Illuminate\Http\Response
     */
    public function destroy(TopUp $topUp)
    {
        //
    }

    public function datatable(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'sender_id', 'status'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = TopUp::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = TopUp::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();

        foreach ($dataset as $key => $item) {
            $status = $this->generateStatusLabel($item->status);
            $invoice_url = route('invoices.show', [$item->invoice_id]);
            $btnActivate = null;
//            $upload_documents = ($item->status == 'pending_documents') ? "<button  onclick=\"uploadDocuments(this)\" data-id='{$item->id}'  class='btn btn-xs  btn-light-warning mr-2'> <i class='fa fa-upload'></i>Upload Documents</button>" : null;
            $make_payment = ($item->status == 'pending_documents') ? "<button onclick=\"paymentTopupId(this)\" data-id='{$item->id}'  data-invoice ='{$item->invoice}'  data-user='{$item->user}' class='btn btn-xs text-nowrap btn-danger mr-2' > <i class='fa fa-dollar-sign'></i>Make Payment</button>" : null;
            $invoice = "<a  target='_blank' href='$invoice_url' class='btn btn-xs btn-light-info mr-2' data-toggle='tooltip' data-placement='top' title='View Invoice'> <i class='fa fa-file-invoice-dollar'></i>View Invoice</a>";

            if ($user->can('top up activate')) {
                if ($item->status != 'paid') {
                    if ($user->hasRole(['Admin'])) {
                        if (($item->is_paid || $item->is_free) && $item->status == 'approving') {
                            $btnActivate = "<button  onclick=\"activateTopupID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Activate</button>";
                        }
                    } else {
                        $btnActivate = "<button  onclick=\"activateTopupID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Activate</button>";
                    }
                }

            }


            $data[$i] = array(
                $item->id,
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                $status,
                $item->amount,
                $item->balance,
                optional($item->business)->name,
                optional($item->user)->name,
                $make_payment . $btnActivate . $invoice
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function generateStatusLabel($status)
    {
        $label = null;
        $text = ucfirst(str_replace('_', ' ', $status));
        switch ($status) {
            case 'pending_documents':
            case 'pending_payment':
                $label = "<label class='label font-weight-bold label-lg  label-light-danger label-inline'>$text</label>";
                break;
            case 'paid':
                $label = "<label class='label font-weight-bold label-lg  label-info label-inline'>$text</label>";
                break;
            case 'credited':
                $label = "<label class='label font-weight-bold label-lg  label-successr label-inline'>$text</label>";
                break;
            case 'approving':
                $label = "<label class='label font-weight-bold label-lg  label-primary label-inline'>$text</label>";
                break;
        }
        return $label;
    }

    public function uploadReceipt(Request $request, TopUp $topUp)
    {

        $topUp->update([
            'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'payment_method' => "bank_transfer",
            'status' => "approving",
        ]);

        $invoice = Invoice::where('id', $topUp->invoice_id)->first();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $destinationPath = storage_path() . '/topup/receipt/';
            $file->move($destinationPath, $fileName);

            $invoice->update([
                'receipt_path' => '/topup/receipt/' . $fileName,
                'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'paid_by' => Auth::user()->id,
                'payment_method' => "bank_transfer",
                'status' => "approving",
            ]);

            return $this->sendResponse($fileName, 'Document Uploaded Successfully');

        } else {
            return $this->sendError('Document Uploaded Failed');
        }
    }

    public function activate(Request $request, TopUp $topUp)
    {
        if (Auth::user() && Auth::user()->can('topup-id activate')) {
            $topUp->status = 'paid';
            $topUp->approved_at = Carbon::now()->format('Y-m-d H:i:s');
            $topUp->approved_by = Auth::id();
            $topUp->save();

            $invoice = Invoice::where('id', $topUp->invoice_id)->first();
            $invoice->status = 'paid';
            $invoice->approved_by = Auth::id();
            $invoice->approved_at = Carbon::now()->format('Y-m-d H:i:s');
            $invoice->save();

            $business = Business::where('id', $invoice->business_id)->first();
            $_balance = $business->balance;
            $new_balance = ($_balance + $invoice->amount);

            $business->update([
                'balance' => $new_balance,

            ]);
            Mail::to($topUp->business->owner)->queue(new SenderIdActivated($topUp));
            return $this->sendResponse($topUp, 'Sender ID Activated Successfully!');
        } else {
            return $this->sendError('Permission Denied');
        }
    }


}
