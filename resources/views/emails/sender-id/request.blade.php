@component('mail::message')
Dear {{$sender_id->business->owner->name}} ,

Your requested Sender ID {{$sender_id->sender_id}} processing started.
Please  Upload required documents and make sure payment has been done.



@component('mail::button', ['url' => route('sender-id.index')])
View Process
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
