
<div class="leftbar-tab-menu">
    <div class="main-icon-menu">
        <a href="#" class="logo logo-metrica d-block text-center">
            <span> <img src="{{asset('assets/images/logo-sm.png')}}" alt="logo-small" class="logo-sm"></span>
        </a>
        <nav class="nav">
            <a href="#Menu1" class="nav-link" data-toggle="tooltip-custom" data-placement="right" title="" data-original-title="Menu" data-trigger="hover">
                <i data-feather="shopping-cart" class="align-self-center menu-icon icon-dual"></i>
            </a>
            <a href="#Menu2" class="nav-link" data-toggle="tooltip-custom"
               data-placement="right" title="" data-original-title="Category"
               data-trigger="hover">
                <i data-feather="grid" class="align-self-center menu-icon icon-dual"></i>
            </a>

            <a href="#Menu3" class="nav-link" data-toggle="tooltip-custom"
               data-placement="right" title="" data-original-title="Menu3"
               data-trigger="hover">
                <i data-feather="package" class="align-self-center menu-icon icon-dual"></i>
            </a>

            <a href="#Menu4" class="nav-link" data-toggle="tooltip-custom"
               data-placement="right" title="" data-original-title="Menu4"
               data-trigger="hover"><i data-feather="copy"
                                       class="align-self-center menu-icon icon-dual"></i>
            </a>
            <a href="#Menu5" class="nav-link" data-toggle="tooltip-custom"
               data-placement="right" title="" data-original-title="Menu5"
               data-trigger="hover">
                <i data-feather="lock" class="align-self-center menu-icon icon-dual"></i>
            </a>
        </nav>

        <div class="pro-metrica-end">
            <a href="#" class="help" data-toggle="tooltip-custom" data-placement="right" title="" data-original-title="Chat">
                <i data-feather="message-circle" class="align-self-center menu-icon icon-md icon-dual mb-4"></i>
            </a>
            <a href="#" class="profile">
                <img src="../assets/images/users/user-1.jpg" alt="profile-user" class="rounded-circle thumb-sm">
            </a>
        </div>
    </div>

    <div class="main-menu-inner">

        <div class="topbar-left">
            <a href="ecommerce-index.html" class="logo">
                <span>
                    Cyntrek
{{--                    <img src="../assets/images/logo-dark.png" alt="logo-large" class="logo-lg logo-dark"> --}}
                    {{--                    <img src="../assets/images/logo.png" alt="logo-large" class="logo-lg logo-light">--}}
                </span>
            </a>
        </div>

        <div class="menu-body slimscroll">
            <div id="Menu1" class="main-icon-menu-pane">
                <div class="title-box"><h6 class="menu-title">Menu</h6></div>
                <ul class="nav">
                    <li class="nav-item"><a class="nav-link" href="#">Dashboard</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Products</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Product List</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Product Detail</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Cart</a></li>
                    <li class="nav-item"><a class="nav-link" href="#">Checkout</a></li>
                </ul>
            </div>

            <div id="Menu2" class="main-icon-menu-pane">
                <div class="title-box"><h6 class="menu-title">Category</h6></div>
                <ul class="nav metismenu">
                    <li class="nav-item"><a class="nav-link" href="{{route('category.index')}}">Category List</a></li>
                </ul>
            </div>

            <div id="Menu3" class="main-icon-menu-pane">
                <div class="title-box"><h6 class="menu-title">Menu 3</h6></div>
                <ul class="nav metismenu">

                </ul>
            </div>


            <div id="Menu4" class="main-icon-menu-pane">
                <div class="title-box"><h6 class="menu-title">Menu 4</h6></div>
                <ul class="nav">

                </ul>
            </div>

            <div id="Menu5" class="main-icon-menu-pane">
                <div class="title-box"><h6 class="menu-title">Menu 5</h6></div>
                <ul class="nav">

                </ul>
            </div>
        </div>
    </div>
</div>
