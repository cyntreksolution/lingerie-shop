@if (Auth::user()->hasRole(['Admin','Super Admin']) )
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Business </label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::select('business_id', $business,null, ['class' => 'form-control','placeholder'=>'Select Business','autocomplete'=>'off','id'=>'business_id','required']) !!}

            </div>
        </div>
    </div>
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Business Section</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::select('business_section_id', $businessSection,null, ['class' => 'form-control','placeholder'=>'Select Business Section','autocomplete'=>'off','id'=>'business_section_id','required']) !!}

{{--                <select name="business_section_id" id="business_section_id" class="form-control" >--}}
{{--                    <option>--Business Section--</option>--}}
{{--                </select>--}}
            </div>
        </div>
    </div>
@else
    <div class="form-group row">
        <label class="col-form-label text-right col-lg-3 col-sm-12">Business Section</label>
        <div class="col-lg-9 col-md-9 col-sm-12">
            <div class="input-group">
                {!! Form::select('business_section_id', $businessSection,null, ['class' => 'form-control','placeholder'=>'Select Business Section','autocomplete'=>'off','id'=>'business_id','required']) !!}

            </div>
        </div>
    </div>
    {!! Form::hidden('business_id', Auth::user()->business->id) !!}
@endif

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Email</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>'Enter Email','autocomplete'=>'off','id'=>'email','required']) !!}
        </div>
    </div>
</div>

@if (empty($edit))
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Password</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('password', null, ['class' => 'form-control','placeholder'=>'Enter Password','autocomplete'=>'off','id'=>'password','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Confirm Password</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('confirmpassword', null, ['class' => 'form-control','placeholder'=>'Enter Confirm Password','autocomplete'=>'off','id'=>'confirmpassword','required']) !!}
        </div>
    </div>
</div>
@endif

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Roles</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('roles[]', $roles,null, ['class' => 'form-control','id'=>'roles','required',]) !!}
         </div>
    </div>
</div>
