<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Business;
use App\Models\Campaign;
use App\Models\Category;
use App\Models\Item;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $count =[];
        $count['items'] =Item::count();
        $count['brands'] =Brand::count();
        $count['category'] =Category::count();
        return view('dashboard',compact('count'));
    }

    public function home()
    {
        $categories = Category::with('products')->get();
        return view('welcome', compact('categories'));
    }

    public function shop()
    {
        return view('shop');
    }

    public function product(Request $request,Item $product)
    {
        $categories = Category::all();
        return view('product',compact('product','categories'));
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }
}
