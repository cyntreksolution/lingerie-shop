<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Status</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::select('sms_status',['pending'=>'Pending','sent'=>'Sent','delivered'=>'Delivered','failed'=>'Failed'],null, ['class' => 'form-control','placeholder'=>'','autocomplete'=>'off','id'=>'sms_status']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Delivered AT</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::date('delivered_at', null, ['class' => 'form-control','placeholder'=>'','autocomplete'=>'off','id'=>'delivered_at']) !!}
        </div>
    </div>
</div>


