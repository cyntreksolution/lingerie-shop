<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable =['name','slug','description'];
    public function scopeTableData($query, $order_column, $order_by_str, $start, $length)
    {
        return $query
            ->select('items.*')
            ->orderBy($order_column, $order_by_str)
            ->offset($start)
            ->limit($length);
    }
    public function scopeSearchData($query, $term)
    {
        return $query
            ->orWhere('items.id', 'like', "%" . $term . "%")
            ->orWhere('items.name', 'like', "%" . $term . "%");
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
}
