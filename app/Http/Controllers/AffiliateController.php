<?php

namespace App\Http\Controllers;

use App\Models\Affiliate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AffiliateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('affiliate.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Affiliate  $affiliate
     * @return \Illuminate\Http\Response
     */
    public function show(Affiliate $affiliate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Affiliate  $affiliate
     * @return \Illuminate\Http\Response
     */
    public function edit(Affiliate $affiliate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Affiliate  $affiliate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Affiliate $affiliate)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Affiliate  $affiliate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Affiliate $affiliate)
    {
        //
    }

    public function datatable(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'owner_id', 'owner_business_id', 'affiliate_user_id','created_at', 'is_paid'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Affiliate::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Affiliate::count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $user = Auth::user();

        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            $btnActivate = null;

            $status = $this->generateStatusLabel($item->is_paid);

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    optional($item->owner)->name,
                    optional($item->business)->name,
                    optional($item->affiliate)->name,
                    optional($item->owner)->mobile,
                    Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                    $status,

                );
            } else {
                $data[$i] = array(
                    $item->id,
                    optional($item->affiliate)->name,
                    optional($item->affiliate)->mobile,
                    Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                    $status,

                );

            }
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
    public function generateStatusLabel($status)
    {
        $label = null;

        switch ($status) {
            case '0':
                $label = "<label class='label font-weight-bold label-lg  label-warning label-inline'>Pending</label>";
                break;
            case '1':
                $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>Paid</label>";
                break;

        }
        return $label;
    }
}
