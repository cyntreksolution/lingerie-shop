<?php

namespace App\Http\Controllers;

use App\Models\BusinessPackageSubscription;
use App\Models\Package;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PackageSubscriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('subscriptions.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        if (Auth::user()->hasAnyRole(['Super Admin', 'Admin'])) {
            $columns = ['id', 'business_id', 'purchased_by', 'package_id', 'start_date', 'end_date', 'price', 'sms_qty', 'available_sms_qty', 'status', 'status'];
        } else {
            $columns = ['id','package_id', 'start_date', 'end_date', 'price', 'sms_qty', 'available_sms_qty', 'status', 'status'];
        }

        $order_column = $columns[$order_by[0]['column']];
        $dataset = BusinessPackageSubscription::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = BusinessPackageSubscription::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();

        foreach ($dataset as $key => $item) {
            $status =null;
            switch ($item->status){
                case 'active':
                    $status = "<button  class='btn btn-xs btn-light-success mr-2'>Active</button>";
                    break;
                case 'finished':
                    $status = "<button  class='btn btn-xs btn-light-warning mr-2'>Finished</button>";
                    break;
                case 'cancelled':
                    $status = "<button  class='btn btn-xs btn-light-danger mr-2'>Cancelled</button>";
                    break;
                case 'expired':
                    $status = "<button  class='btn btn-xs btn-light-danger mr-2'>Expired</button>";
                    break;
            }

            $invoice_url = route('invoices.show', [$item->invoice_id]);
            $invoice = "<a  target='_blank' href='$invoice_url' class='btn btn-xs btn-light-info mr-2'> <i class='fa fa-file-invoice-dollar mr-2'></i></a>";

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    $item->business->name,
                    $item->purchasedBy->name,
                    $item->package->name,
                    $item->start_date,
                    $item->end_date,
                    number_format($item->price,2),
                    $item->sms_qty,
                    $item->available_sms_qty,
                    Carbon::parse($item->end_date)->diffForHumans(Carbon::now()),
                    $status,
                    $invoice
                );
            } else {
                $data[$i] = array(
                    $item->id,
                    $item->package->name,
                    $item->start_date,
                    $item->end_date,
                    number_format($item->price,2),
                    $item->sms_qty,
                    $item->available_sms_qty,
                    Carbon::parse($item->end_date)->diffForHumans(Carbon::now()),
                    $status,
                    $invoice
                );
            }

            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
