@extends('layouts.master')
@section('title','Campaign List')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Campaign List
                            <span class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} Campaign list</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{route('sms-campaigns.index')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Create New SMS Campaign">
                            <i class="fa fa-mail-bulk mr-1"></i> New SMS Campaign
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Campaign Name</th>
                            <th>Sender ID</th>
                            <th>Message Content</th>
                            <th>Campaign Status</th>
                            <th>Instant / Scheduled</th>
                            <th>Date & Time</th>
                            <th>Pages</th>
                            <th>Count</th>
                            <th>Charge</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="filterModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Filter</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'contacts.store', 'method' => 'post','id'=>'filterForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('sms.campaign.cfilter')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="filterForm('filterForm','filterModal')"
                            type="button" class="btn btn-success"><i class="fa fa-filter mr-1"></i> Filter
                    </button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>
        DataTableOption.initDataTable('datatable', '/sms-campaigns/data/table',true,[0,'desc']);

        function submitForm(form_id, modal_id, table_id) {
            validateFormNow(form_id).validate().then(function (status) {
                if (status == "Valid") {
                    form_id = '#' + form_id;
                    table_id = '#' + table_id;
                    let url = $(form_id).attr('action');
                    let method = $(form_id).attr('method');

                    $(form_id).ajaxSubmit(
                        {
                            clearForm: true,
                            url: url,
                            type: method,
                            success: function (result) {
                                ModalOptions.hideModal(modal_id);
                                if (result.success) {
                                    let table = $(table_id).DataTable();
                                    table.ajax.reload();
                                    Notifications.showSuccessMsg(result.message);
                                } else {
                                    Notifications.showErrorMsg(result.message);
                                }

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                ModalOptions.hideModal(modal_id);
                                Notifications.showErrorMsg(errorThrown);
                            }
                        }
                    );
                }
            });
        }

        function filterForm(form_id, modal_id) {
            let modal ='#'+modal_id;
            let status = $('#sms_status').val();
            let delivered_at = $('#delivered_at').val();

            let table = $('#datatable').DataTable();
            table.ajax.url('/sms-campaigns/data/table?status=' + status + '&delivered_at=' + delivered_at + '&filter='+true).load();
            $(modal).modal('toggle');

        }

        $("#datatable_filter").append("<button class='btn btn-outline-primary ml-1 ' data-toggle='modal' data-target='#filterModal'> <i class='fa fa-filter'/> </button>")


    </script>
@endpush
