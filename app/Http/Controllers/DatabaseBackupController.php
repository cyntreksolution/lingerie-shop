<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\Campaign;
use App\Models\DatabaseBackup;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

class DatabaseBackupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('database.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $filename = "backup_" . Carbon::now()->format('YmdHis') . ".sql";


        $command = "mysqldump  --user=" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  > " . public_path() . "/backup/" . $filename;


        $returnVar = NULL;
        $output = NULL;


        exec($command, $output, $returnVar);


        $file_size =File::size(public_path() . "/backup/" . $filename);

        $db = new DatabaseBackup();
        $db->file_path =  $filename;
        $db->size = $this->bytesToHuman($file_size);
        $db->save();

        return redirect(route('database-backup.index'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\DatabaseBackup $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function show(DatabaseBackup $databaseBackup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\DatabaseBackup $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function edit(DatabaseBackup $databaseBackup)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\DatabaseBackup $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DatabaseBackup $databaseBackup)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\DatabaseBackup $databaseBackup
     * @return \Illuminate\Http\Response
     */
    public function destroy(DatabaseBackup $databaseBackup)
    {
        $file= public_path() . "/backup/" . $databaseBackup->file_path;
        File::delete($file);
        $databaseBackup->delete();
        return $this->sendResponse('','File Deleted');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'created_at', 'size','download_count'];

        $order_column = $columns[$order_by[0]['column']];

        $dataset = DatabaseBackup::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = DatabaseBackup::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;


        $user = Auth::user();
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            if ($can_delete) {
                $url = "'database-backup/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }

            $url = route('db.download',$item->id);
            $file  = "backup/$item->file_path";
            $btnDownload = "<a href='$url'  target='_blank' class='btn btn-xs mr-2 btn-icon btn btn-primary'><i class='fa fa-download'></i> </a>";


            $data[$i] = array(
                $item->id,
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                $item->size,
                $item->download_count,
                $btnDownload.$delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function downloadFile(Request  $request,$id){
        $dbfile = DatabaseBackup::find($id);

        $count =$dbfile->download_count;
        $dbfile->download_count = $count +1;
        $dbfile->save();

        $file= public_path() . "/backup/" . $dbfile->file_path;



        $headers = array(
            'Content-Type: application/sql',
        );


        return Response::download($file, $dbfile->file_path, $headers);

    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
