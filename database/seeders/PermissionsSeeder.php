<?php

namespace Database\Seeders;

use App\Models\Permission;
use App\Models\PermissionGroup;
use Illuminate\Database\Seeder;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group = PermissionGroup::firstOrCreate(['name' => 'Users']);
        Permission::firstOrCreate(['permission_group_id' => $group->id, 'name' => 'users index', 'guard_name' => 'web']);
        Permission::firstOrCreate(['permission_group_id' => $group->id, 'name' => 'users create', 'guard_name' => 'web']);
        Permission::firstOrCreate(['permission_group_id' => $group->id, 'name' => 'users edit', 'guard_name' => 'web']);
        Permission::firstOrCreate(['permission_group_id' => $group->id, 'name' => 'users destroy', 'guard_name' => 'web']);
    }
}
