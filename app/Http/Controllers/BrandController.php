<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('brand.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $category = Brand::create([
            'name' => $request->input('name'),
            'slug' => Str::slug($request->input('name'),'-'),
            'description' => $request->input('description')
        ]);

        $msg = 'Brand Created Successfully';
        return $this->sendResponse($category, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
        ]);

        $brand->name = $request->input('name');
        $brand->slug = Str::slug($request->input('name'),'-');
        $brand->description = $request->input('description');
        $brand->save();

        $msg = 'Category Updated Successfully';
        return $this->sendResponse($brand, $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        $brand->delete();
        return $this->sendResponse([], 'Brand Deleted');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'name', 'description'];
        $order_column = $columns[$order_by[0]['column']];

        $query = Brand::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $query = $query->get();
            $data_count = Brand::all()->count();
        } else {
            $query = $query->searchData($search)->get();
            $data_count = $query->count();
        }

        $data = [];
        $i = 0;

        $can_edit = $can_delete = 1;

        foreach ($query as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-description='{$item->description}' ><i class='fa fa-pencil-alt '  ></i></button>";
            }
            if ($can_delete) {
                $url = "'brands/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }

            $data[$i] = array(
                $item->id,
                $item->name,
//                $item->description,
                $edit_btn . $delete_btn
            );


            $i++;
        }


        if ($data_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($data_count),
            "recordsFiltered" => intval($data_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
