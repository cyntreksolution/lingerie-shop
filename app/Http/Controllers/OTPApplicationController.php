<?php

namespace App\Http\Controllers;

use App\Models\OTPApplication;
use App\Models\SenderID;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class OTPApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sender_id = SenderID::whereStatus('active')->pluck('sender_id', 'id');
        return view('otp-app.index', compact('sender_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'app_name' => 'required',
            'sender_id' => 'required',
            'validity_period' => 'required',
            'digits' => 'required',
            'otp_message' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err_str = '';
            foreach ($errors as $error) {
                $err_str .= $error . " <br> <br> ";
            }
            return $this->sendError($err_str, 'Required Fields Missing !');
        }

        $status = $request->status == 1?1:0;
        $oa = new OTPApplication();
        $oa->app_name= $request->app_name;
        $oa->business_id =$request->business_id;
        $oa->sender_id =$request->sender_id;
        $oa->validity_period =$request->validity_period;
        $oa->digits =$request->digits;
        $oa->callback_url =$request->callback_url;
        $oa->otp_message =$request->otp_message;
        $oa->app_key = rand(100001, 999999);
        $oa->app_secret = Str::random(32);
        $oa->status =$status;



        $oa->save();

        return $this->sendResponse($oa, 'OTP App Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\OTPApplication $oTPApplication
     * @return \Illuminate\Http\Response
     */
    public function show(OTPApplication $oTPApplication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\OTPApplication $oTPApplication
     * @return \Illuminate\Http\Response
     */
    public function edit(OTPApplication $oTPApplication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\OTPApplication $oTPApplication
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OTPApplication $otpApp)
    {

        $rules = array(
            'app_name' => 'required',
            'sender_id' => 'required',
            'validity_period' => 'required',
            'digits' => 'required',
            'otp_message' => 'required',
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err_str = '';
            foreach ($errors as $error) {
                $err_str .= $error . " <br> <br> ";
            }
            return $this->sendError($err_str, 'Required Fields Missing !');
        }

        $status = $request->status == 1?1:0;


        $otpApp->app_name= $request->app_name;
        $otpApp->business_id =$request->business_id;
        $otpApp->sender_id =$request->sender_id;
        $otpApp->validity_period =$request->validity_period;
        $otpApp->digits =$request->digits;
        $otpApp->callback_url =$request->callback_url;
        $otpApp->otp_message =$request->otp_message;
        $otpApp->status =$status;
        $otpApp->save();

        return $this->sendResponse($otpApp, 'OTP App Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\OTPApplication $oTPApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(OTPApplication $oTPApplication)
    {
        //
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'app_name', 'sender_id', 'validity_period', 'digits', 'callback_url', 'otp_message'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = OTPApplication::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = OTPApplication::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }
        //  dd($dataset);
        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('otp app edit')) ? 1 : 0;
        $can_delete = ($user->can('otp app destroy')) ? 1 : 0;


        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-digits='{$item->digits}' data-callback_url='{$item->callback_url}' data-status='{$item->status}' data-validity_period='{$item->validity_period}' data-app_name='{$item->app_name}' data-sender_id='{$item->sender_id}' data-otp_message='{$item->otp_message}' ></i>";
            }
            if ($can_delete) {
                $url = "'users/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }


            $status = $item->status ==1 ?"<label class='btn btn-xs btn-light-success mr-2'>Enabled</label>":"<label class='btn btn-xs btn-light-danger mr-2'>Disabled</label>";

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = [
                    $item->id,
                    $item->business->name,
                    $item->app_name,
                    $item->senderId->sender_id,
                    $item->validity_period,
                    $item->digits,
                    $item->callback_url,
                    $status,
                    $edit_btn.$delete_btn
                ];
            } else {
                $data[$i] = [
                    $item->id,
                    $item->app_name,
                    $item->senderId->sender_id,
                    $item->validity_period,
                    $item->digits,
                    $item->callback_url,
                    $status,
                    $edit_btn.$delete_btn
                ];
            }


            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
