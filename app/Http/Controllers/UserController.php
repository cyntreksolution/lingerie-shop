<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\BusinessSection;
use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Arr;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data = User::orderBy('id', 'DESC');
        $user = Auth::user();


        $roles = Role::pluck('name', 'name');
        $business_id = Auth::user()->business_id;
        $business = Business::pluck('name', 'id')->all();
        $businessSection = BusinessSection::pluck("name", "id");

        //$userRole = $data->roles->pluck('name','name')->all();
        return view('users.index', compact('data', 'roles', 'business', 'businessSection'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirmpassword',
            'roles' => 'required',
            'business_section_id' => 'required',
            'business_id' => 'required',
        ]);
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);
        $user->assignRole($request->input('roles'));

        $msg = 'User Created Successfully';
        return $this->sendResponse($user, $msg);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'first_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'business_section_id' => 'required',
            'business_id' => 'required',
        ]);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->name = $request->first_name . ' ' . $request->last_name;
        $user->mobile = $request->mobile;
        $user->business_id = $request->business_id;
        $user->business_section_id = $request->business_section_id;

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/user_profile/';
            $fileName = $user->id . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $fileName);
            $user->profile_photo_path = '/user_profile/' . $fileName;
        }

        $user->syncRoles([$request->input('roles')]);

        $user->save();
        return redirect(route('user.profile', $user->id));

    }
    public function updateByAdmin(Request $request, User $user)
    {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'business_section_id' => 'required',
            'business_id' => 'required',
        ]);


        $user->name = $request->name;
        $user->email = $request->email;
        $user->business_id = $request->business_id;
        $user->business_section_id = $request->business_section_id;


        $user->syncRoles([$request->input('roles')]);

        $user->save();
        return $this->sendResponse('', 'User Successfully Updated');
    }

    function updatePassword(Request $request, User $user)
    {
        $rules = array(
            'current_password' => 'required',
            'password' => ['required', 'string', 'min:8', 'confirmed', 'different:current_password'],
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err_str = '';
            foreach ($errors as $error) {
                $err_str .= $error;
            }

            $request->session()->flash('alert-type', 'error');
            $request->session()->flash('message', $err_str);
            return redirect(route('user.profile', $user->id));
        }

        if (Hash::check($request->current_password, $user->password)) {
            $user->fill([
                'password' => Hash::make($request->password)
            ])->save();

            $request->session()->flash('alert-type', 'success');
            $request->session()->flash('message', 'Password Changed Successfully');
            return redirect(route('user.profile', $user->id));

        } else {
            $request->session()->flash('alert-type', 'error');
            $request->session()->flash('message', 'Password does not match');
            return redirect(route('user.profile', $user->id));
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return $this->sendResponse('', 'User Successfully Deleted');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['name', 'email', 'roles'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = User::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = User::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->can('users edit')) ? 1 : 0;
        $can_delete = ($user->can('users delete')) ? 1 : 0;

        foreach ($dataset as $key => $item) {

            if ($can_edit && $item->id != Auth::id()) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-email='{$item->email}' data-business_id='{$item->business_id}' data-business_section_id='{$item->business_section_id}'  data-role='{$item->roles[0]->name}' ></i>";
            }
            if ($can_delete) {
                $url = "'users/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"></i>";
            }


            $data[$i] = array(
                $item->id,
                $item->name,
                $item->email,
                optional($item->business)->name,
                optional($item->businessSection)->name,
                $item->getRoleNames(),
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function profile(Request $request, User $user)
    {
        return view('users.profile', compact('user'));
    }

    public function getBusinessSection($id)
    {
        $businessSection = BusinessSection::where("business_id", $id)->pluck("name", "id");
        return json_encode($businessSection);
    }
}
