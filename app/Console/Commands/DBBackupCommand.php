<?php

namespace App\Console\Commands;

use App\Models\DatabaseBackup;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class DBBackupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:backup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $filename = "system_backup_" . Carbon::now()->format('YmdHis') . ".sql";
        $command = "mysqldump  --user=" . env('DB_USERNAME') . " --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  > " . public_path() . "/backup/" . $filename;
        $returnVar = NULL;
        $output = NULL;
        exec($command, $output, $returnVar);

        $file_size =File::size(public_path() . "/backup/" . $filename);


        $db = new DatabaseBackup();
        $db->file_path =  $filename;
        $db->size = $this->bytesToHuman($file_size);
        $db->save();

    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];

        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }

        return round($bytes, 2) . ' ' . $units[$i];
    }
}
