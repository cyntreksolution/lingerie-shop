<?php

namespace App\Http\Controllers;

use App\Jobs\GetDeliveryStatusJob;
use App\Mail\InvoiceCreatedMail;
use App\Models\Business;
use App\Models\BusinessSection;
use App\Models\Campaign;
use App\Models\CampaignNumber;
use App\Models\CampaignPackage;
use App\Models\Invoice;
use App\Models\SenderID;
use App\Models\SMS;
use App\Models\User;
use App\Traits\SendSMS;
use App\Traits\SMSPriceTrait;
use App\Traits\SMSTrait;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class DebugController extends Controller
{
    use SendSMS;
    use SMSTrait;
    use  SMSPriceTrait;

    public function index()
    {

    }
}
