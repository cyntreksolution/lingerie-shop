<?php

namespace App\Http\Controllers;

use App\Events\BulkCampaignCreated;
use App\Models\Business;
use App\Models\Campaign;
use App\Models\CampaignContact;
use App\Models\CampaignNumber;
use App\Models\Contact;
use App\Models\ScheduledCampaign;
use App\Models\SenderID;
use App\Models\SMS;
use App\Traits\SendSMS;
use App\Traits\SMSPriceTrait;
use App\Traits\SMSTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class SMSController extends Controller
{
    use SendSMS;
    use SMSTrait;
    use SMSPriceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isDemoUser()) {
            $sender_ids = SenderID::withoutGlobalScopes(['company'])->where('sender_id', '=', 'KentacyDEMO')->pluck('sender_id', 'id');
        } else {
            $sender_ids = SenderID::where('status', '=', 'active')->pluck('sender_id', 'id');
        }
        $business_id = Auth::user()->business_id;
        $contacts = Contact::whereBusinessId($business_id)->pluck('first_name', 'id');
        return view('sms.send', compact('sender_ids', 'contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'sender_id' => 'required',
            'message' => 'required',
            'number' => 'required_without_all:contact',
            'contact' => 'required_without_all:number',
        );

        $validator = Validator::make($request->all(), $rules);

        if (!empty($request->number)) {
            $rules = array(
                'number' => 'digits_between:9,12',
            );
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                $err_str = '';
                foreach ($errors as $error) {
                    $err_str .= $error . " <br> <br> ";
                }
                return $this->sendError($err_str, 'Required Fields Missing !');
            }
        }

        if ($validator->fails()) {
            $errors = $validator->errors()->all();
            $err_str = '';
            foreach ($errors as $error) {
                $err_str .= $error . " <br> <br> ";
            }
            return $this->sendError($err_str, 'Required Fields Missing !');
        }

        $number = $request->number;
        $contact = $request->contact;
//        $is_schedule = $request->is_schedule;
//        $schedule_time = $request->schedule_time;
        $sender_id = $request->sender_id;
        $message = $request->message;
        $business = Auth::user()->business;



        $campaign = new Campaign();
        $campaign->business_id = $business->id;
        $campaign->user_id = Auth::id();
        $campaign->sender_id = $sender_id;
        $campaign->type = 'individual';
        $campaign->message = $message;
//        $campaign->is_scheduled = !empty($is_schedule) ? 1 : 0;
//        $campaign->scheduled_time = !empty($is_schedule) ? Carbon::parse($schedule_time)->format('Y-m-d H:i') : null;
        $campaign->status = 'pending';
        $campaign->sms_count = 0;
        $campaign->page_count = $this->count($message)->messages;
        $campaign->cost_per_sms = $business->sms_charge;
        $campaign->start_balance = 0;
        $campaign->sms_charge = 0;
        $campaign->end_balance = 0;

        $campaign->save();

        if (!empty($number)) {
            $cn = CampaignNumber::firstOrCreate(['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $number]);
        }

        if (!empty($contact)) {
            $selected_contact = Contact::find($contact);
            $contact = new CampaignContact();
            $contact->business_id = $business->id;
            $contact->campaign_id = $campaign->id;
            $contact->contact_id = $selected_contact->id;
            $contact->save();
            $cn = CampaignNumber::firstOrCreate(
                ['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $selected_contact->number],
                ['contact_id' => $selected_contact->id]);

        }

        $number_count = CampaignNumber::whereCampaignId($campaign->id)->count();
        $campaign->sms_count = $number_count;
        $campaign->save();



        if ($this->hasActiveSubscription($business)) {
            $created = BulkCampaignCreated::dispatch($campaign);
            return $this->sendResponse($created, 'SMS Created Successfully');
        }elseif ($campaign->sms_charge < $business->balance){
            $charge = ($campaign->sms_count * $campaign->page_count) * $campaign->cost_per_sms;
            $real_cost = ($campaign->sms_count * $campaign->page_count) * 0.62;
            $profit = $charge - $real_cost;

            $campaign->profit = $profit;
            $campaign->sms_charge = $charge;
            $campaign->save();
            $created = BulkCampaignCreated::dispatch($campaign);
            return $this->sendResponse($created, 'SMS Created Successfully');
        }else{
            $campaign->status = 'failed';
            $campaign->error_code = 'insufficient_balance';
            $campaign->save();
            return $this->sendError('Your Account Balance Is Insufficient . Please Top UP');
        }




    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\SMS $sMS
     * @return \Illuminate\Http\Response
     */
    public function show(SMS $sMS)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SMS $sMS
     * @return \Illuminate\Http\Response
     */
    public function edit(SMS $sMS)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SMS $sMS
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SMS $sMS)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SMS $sMS
     * @return \Illuminate\Http\Response
     */
    public function destroy(SMS $sMS)
    {
        //
    }


    public function listView()
    {
        return view('sms.individual.index');
    }

    public function apiDatatableView()
    {
        return view('sms.api.index');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['campaigns.created_at', 'name', 'sender_ids.sender_id', 'message', 'campaigns.status', 'is_scheduled', 'scheduled_time', 'sms_charge'];

        $order_column = $columns[$order_by[0]['column']];
        $dataset = Campaign::whereType('individual')->tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Campaign::whereType('individual')->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;


        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;
            if ($can_edit) {
                if ($item->is_scheduled && $item->status == 'pending') {
                    $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}'><i class='fa fa-pencil-alt'  ></i></button>";
                }
            }
            if ($can_delete) {
                $url = "'contacts/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"><i class='fa fa-trash' ></i></button>";
            }

            $show_btn = "<button class='btn btn-xs btn-icon btn-light-success mr-2'><i class='fa fa-eye'></i></button>";


            $scheduled = ($item->is_scheduled) ? "<label class='label label-lg font-weight-bold label-light-dark label-inline'>Scheduled</label>" : "<label class='label label-lg font-weight-bold label-light-primary label-inline'>Instant</label>";

            $status = null;

            switch ($item->status) {
                case 'pending':
                    $status = "<label class='label label-lg font-weight-bold label-light-warning label-inline'>Pending</label>";
                    break;
                case 'running':
                    $status = "<label class='label label-lg font-weight-bold label-light-info label-inline'>Running</label>";
                    break;
                case 'sent':
                    $status = "<label class='label label-lg font-weight-bold label-light-success label-inline'>Sent</label>";
                    break;
                case 'cancelled':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Cancelled</label>";
                    break;
                case 'failed':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Failed</label>";
                    break;
            }

            $data[$i] = array(
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                optional($item->senderID)->sender_id,
                $item->numbers->pluck('number'),
                $item->message,
                $status,
//                $scheduled,
//                $item->scheduled_time,
                number_format($item->sms_charge, 2) . ' LKR',
//                $edit_btn . $show_btn
                null
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function apiDatatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['campaigns.created_at', 'name', 'sender_ids.sender_id', 'message', 'campaigns.status', 'is_scheduled', 'scheduled_time', 'sms_charge'];

        $order_column = $columns[$order_by[0]['column']];
        $dataset = Campaign::whereType('individual_api')->tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Campaign::whereType('individual_api')->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;


        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;
            if ($can_edit) {
                if ($item->is_scheduled && $item->status == 'pending') {
                    $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}'><i class='fa fa-pencil-alt'  ></i></button>";
                }
            }
            if ($can_delete) {
                $url = "'contacts/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"><i class='fa fa-trash' ></i></button>";
            }

            $show_btn = "<button class='btn btn-xs btn-icon btn-light-success mr-2'><i class='fa fa-eye'></i></button>";


            $scheduled = ($item->is_scheduled) ? "<label class='label label-lg font-weight-bold label-light-dark label-inline'>Scheduled</label>" : "<label class='label label-lg font-weight-bold label-light-primary label-inline'>Instant</label>";

            $status = null;

            switch ($item->status) {
                case 'pending':
                    $status = "<label class='label label-lg font-weight-bold label-light-warning label-inline'>Pending</label>";
                    break;
                case 'running':
                    $status = "<label class='label label-lg font-weight-bold label-light-info label-inline'>Running</label>";
                    break;
                case 'sent':
                    $status = "<label class='label label-lg font-weight-bold label-light-success label-inline'>Sent</label>";
                    break;
                case 'cancelled':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Cancelled</label>";
                    break;
                case 'failed':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Failed</label>";
                    break;
            }

            $data[$i] = array(
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                optional($item->senderID)->sender_id,
                $item->message,
                $status,
//                $scheduled,
//                $item->scheduled_time,
                number_format($item->sms_charge, 2) . ' LKR',
//                $edit_btn . $show_btn
                null
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


}
