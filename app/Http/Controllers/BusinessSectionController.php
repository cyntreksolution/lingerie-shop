<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\BusinessSection;
use Illuminate\Http\Request;

class BusinessSectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $businessSections = Business::pluck('name','id')->all();
        return view('business-sections.index',compact('businessSections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'business_section' => 'required',
        ]);
        $businessSection = new BusinessSection();
        $businessSection->name = $request->name;
        $businessSection->business_id = $request->business_section;
        $businessSection->save();

        return $this->sendResponse($businessSection, 'Business Section Created Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BusinessSection  $businessSection
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessSection $businessSection)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BusinessSection  $businessSection
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessSection $businessSection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BusinessSection  $businessSection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessSection $businessSection)
    {
        $this->validate($request, [
            'name' => 'required',
            'business_section' => 'required',
        ]);
        $businessSection->name = $request->name;
        $businessSection->business_id = $request->business_section;
        $businessSection->save();

        return $this->sendResponse($businessSection, 'Business Section Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BusinessSection  $businessSection
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessSection $businessSection)
    {
        $businessSection->delete();
        return $this->sendResponse($businessSection, 'Business Section Successfully Deleted');
    }
    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'business.name', 'name'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = BusinessSection::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = BusinessSection::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
//        $can_edit = ($user->hasPermissionTo('institute edit')) ? 1 : 0;
        $can_edit = 1;
//        $can_delete = ($user->hasPermissionTo('institute delete')) ? 1 : 0;
        $can_delete = 1;

        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<i class='fa fa-pencil-alt text-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' data-business_section='{$item->business_id}' data-toggle='tooltip' data-placement='top' title='Edit'></i>";
            }
            if ($can_delete) {
                $url = "'business-section/" . $item->id . "'";
                $delete_btn = "<i class='fa fa-trash text-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\" data-toggle='tooltip' data-placement='top' title='Delete'></i>";
            }




            $data[$i] = array(
                $item->id,
                optional($item->business)->name,
                $item->name,
                $edit_btn . $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
