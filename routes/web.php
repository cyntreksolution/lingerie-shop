<?php

use App\Http\Controllers\AffiliateController;
use App\Http\Controllers\AffiliatePayoutController;
use App\Http\Controllers\APIController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\BusinessController;
use App\Http\Controllers\CampaignController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\DatabaseBackupController;
use App\Http\Controllers\DebugController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OTPApplicationController;
use App\Http\Controllers\OTPController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\PackageSubscriptionController;
use App\Http\Controllers\SenderIDController;
use App\Http\Controllers\TopUpController;
use App\Http\Controllers\PaymentController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\SMSController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\PermissionGroupController;
use App\Http\Controllers\PaymentResponseController;
use App\Http\Controllers\BusinessSectionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'home'])->name('home');
Route::get('/shop', [HomeController::class, 'shop'])->name('shop');
Route::get('/about', [HomeController::class, 'about'])->name('about');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');

Route::get('/product/{product}/view', [HomeController::class, 'product'])->name('product');

Auth::routes();
Route::get('/debug', [DebugController::class, 'index'])->name('debug');
Route::group(['middleware' => ['auth']], function () {

    Route::resource('roles', RoleController::class)->middleware('role_or_permission:Super Admin');
    Route::get('roles/data/table', [RoleController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

    Route::resource('permission-groups', PermissionGroupController::class)->middleware('role_or_permission:Super Admin');
    Route::get('permission-groups/data/table', [PermissionGroupController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

    Route::resource('permissions', PermissionController::class)->middleware('role_or_permission:Super Admin');
    Route::get('permissions/data/table', [PermissionController::class, 'datatable'])->middleware('role_or_permission:Super Admin');

    Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard')->middleware('role_or_permission:Super Admin|Admin|Businessman|User');

    Route::resource('categories', CategoryController::class);
    Route::get('categories/data/table', [CategoryController::class, 'datatable']);

    Route::resource('brands', BrandController::class);
    Route::get('brands/data/table', [BrandController::class, 'datatable']);

    Route::resource('products', ItemController::class);
    Route::get('products/data/table', [ItemController::class, 'datatable']);

    Route::resource('users', UserController::class);
    Route::get('users/data/table', [UserController::class, 'datatable']);
    Route::patch('users-admin/{user}', [UserController::class, 'updateByAdmin']);
    Route::get('users/profile/{user}', [UserController::class, 'profile'])->name('user.profile');
    Route::patch('users/profile/{user}/update-password', [UserController::class, 'updatePassword'])->name('update.password');
    Route::get('dropdownlist/business-section/{id}',[UserController::class, 'getBusinessSection']);

    Route::resource('sms', SMSController::class)->middleware('role_or_permission:Super Admin|instant SMS index|instant SMS create');
    Route::get('sms/list/view', [SMSController::class, 'listView'])->name('sms.list')->middleware('role_or_permission:Super Admin|instant SMS index|instant SMS create');
    Route::get('sms/data/table', [SMSController::class, 'datatable'])->name('sms.data')->middleware('role_or_permission:Super Admin|instant SMS index|instant SMS create');
    Route::get('api/sms/view', [SMSController::class, 'apiDatatableView'])->name('api.data')->middleware('role_or_permission:Super Admin|api SMS index');
    Route::get('api/data/table', [SMSController::class, 'apiDatatable'])->name('api.list')->middleware('role_or_permission:Super Admin|api SMS index');

    Route::resource('sms-campaigns', CampaignController::class)->middleware('role_or_permission:Super Admin|campaign SMS index|campaign SMS create');
    Route::get('sms-campaigns/list/view', [CampaignController::class, 'listView'])->name('sms-campaigns.list')->middleware('role_or_permission:Super Admin|campaign SMS index|campaign SMS create');
    Route::get('sms-campaigns/data/table', [CampaignController::class, 'datatable'])->name('sms-campaigns.data')->middleware('role_or_permission:Super Admin|campaign SMS index|campaign SMS create');
    Route::get('sms-campaigns-numbers/data/table/{campaignId}', [CampaignController::class, 'datatable_numbers'])->name('sms-campaigns.numbers')->middleware('role_or_permission:Super Admin|sender-id create');

    Route::resource('database-backup', DatabaseBackupController::class)->middleware('role_or_permission:Super Admin|Admin');
    Route::get('database-backup/data/table', [DatabaseBackupController::class, 'datatable'])->middleware('role_or_permission:Super Admin|Admin');
    Route::get('database-backup/download/file/{id}', [DatabaseBackupController::class, 'downloadFile'])->name('db.download')->middleware('role_or_permission:Super Admin|Admin');

});


Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
