<?php

namespace App\Http\Controllers;

use App\Events\PaymentResponseEvent;
use App\Models\Affiliate;
use App\Models\Business;
use App\Models\Invoice;
use App\Models\PaymentRespons;
use App\Models\SenderID;
use App\Models\TopUp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentResponseController extends Controller
{
    public function payment_response(Request $request)
    {
        $data = $request->result['data'];
        if ($data['status'] == 'SUCCESS') {
            $refNo = $data['reference'];
            $invoice = Invoice::where('invoice_no', $refNo)->first();

            $invoice->update([
                'paid_at' => $data['dateTime'],
                'paid_by' => Auth::user()->id,
                'status' => 'paid',
                'transaction_id' => $data['transactionId'],
                'payment_method' => "online",
            ]);

            if ($invoice->reference_type == 'App/SenderID') {
                $sender_id = SenderID::where('invoice_id', $invoice->id)->first();

                $status = (!empty($sender_id->noc_letter_path)) ? 'approving' : 'pending_documents';

                $sender_id->update([
                    'paid_at' => $data['dateTime'],
                    'payment_method' => "online",
                    'is_paid' => 1,
                    'status' => $status,
                ]);
            } elseif ($invoice->reference_type == 'App/TopUp') {
                $tOpUp = TopUp::where('invoice_id', $invoice->id)->first();

                $tOpUp->update([
                    'paid_at' => $data['dateTime'],
                    'amount' => $data['amount'],
                    'payment_method' => "online",
                    'status' => "paid",

                ]);

                $business = Business::where('id', $tOpUp->business_id)->first();
                $_balance = $business->balance;
                $new_balance = ($_balance + $data['amount']);

                $business->update([
                    'balance' => $new_balance,

                ]);
            } elseif ($invoice->reference_type == 'App/User') {
                $business = Business::find($invoice->business_id);
                $business->is_demo = 0;
                $business->save();

                $business = Business::where('id', $business->id)->first();
                $owner = $business->owner;

                $affiliate = Affiliate::where('affiliate_user_id', '=', $owner->id)->first();

                if (!empty($affiliate) && $affiliate->count() > 0) {
                    $affiliate_business = Business::find($affiliate->owner_business_id);

                    $current_balance = $affiliate_business->affiliate_balance;

                    $affiliate_business->affiliate_balance = $current_balance + $affiliate->amount;
                    $affiliate_business->save();

                    $affiliate->is_paid = 1;
                    $affiliate->start_balance = $current_balance;
                    $affiliate->amount = 500;
                    $affiliate->end_balance = $current_balance + $affiliate->amount;
                    $affiliate->save();
                }
            }
            $invoice = PaymentResponseEvent::dispatch($invoice);

            return $this->sendResponse($invoice, 'Payment Created Successfully');

        }


    }

    public function payment_error_response(Request $request)
    {
        $data = $request->result['data'];

        $refNo = $data['reference'];
        // $invoice = Invoice::where('invoice_no', $refNo)->first();

        $data = PaymentRespons::create([
            'reference_no' => $data['reference'],
            'amount' => $data['amount'],
            'transaction_id' => $data['transactionId'],
            'status' => $data['status'],
            'paid_at' => $data['dateTime'],
        ]);


        //   return $this->sendResponse($sender_id, 'Payment Created Successfully');


    }


}
