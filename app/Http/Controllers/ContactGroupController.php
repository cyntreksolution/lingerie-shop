<?php

namespace App\Http\Controllers;

use App\Imports\ContactsImport;
use App\Models\Contact;
use App\Models\ContactGroup;
use App\Models\ContactGroupContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class ContactGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::pluck('first_name', 'id');
        return view('contact-groups.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $business_id = $request->business_id;
        $contacts = $request->contacts;

        $contact_group = new ContactGroup();
        $contact_group->name = $name;
        $contact_group->business_id = $business_id;
        $contact_group->save();

        $group_id = $contact_group->id;

        if (!empty($request->contacts) && sizeof($request->contacts) > 0) {
            foreach (array_filter($request->contacts) as $contact) {
                $n = new ContactGroupContact();
                $n->contact_group_id = $group_id;
                $n->contact_id = $contact;
                $n->save();
            }
        }


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/tmp_numbers/';
            $fileName = 'temp_numberList' . uniqid() . '.txt';
            $file->move($destinationPath, $fileName);
            $pdo = DB::connection()->getPdo();

            $pdo->beginTransaction();
            $path = str_replace('\\', '/', $destinationPath);


            $lines = file("$path$fileName");
            $line = $lines[0];
            $uniq = rand(0, 999999999);
            switch (TRUE) {
                case substr($line, -2) === "\r\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id ,rand_id = $uniq ";
                    $pdo->exec($query);
                    break;
                case substr($line, -1) === "\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '
' IGNORE 1 LINES (first_name,last_name,number,birthday) SET business_id =  $business_id ,rand_id = $uniq";
                    $pdo->exec($query);
                    break;
                default :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id,rand_id = $uniq ";
                    $pdo->exec($query);
                    break;

            }


            $pdo->commit();

            File::delete($destinationPath . $fileName);

            $contacts = Contact::whereRandId($uniq)->get();
            foreach ($contacts as $contact) {
                $contact->rand_id = null;
                $contact->save();

                $cc = new ContactGroupContact();
                $cc->contact_group_id = $contact_group->id;
                $cc->contact_id = $contact->id;
                $cc->save();

            }

        }

        return $this->sendResponse($contact_group, 'Contact Group Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\ContactGroup $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function show(ContactGroup $contactGroup)
    {
        return view('contact-groups.show', compact('contactGroup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\ContactGroup $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactGroup $contactGroup)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\ContactGroup $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ContactGroup $contactGroup)
    {
        $name = $request->name;
        $contact_group = $contactGroup;
        $business_id = $request->business_id;
        $contact_group->name = $name;
        $contact_group->save();

        $group_id = $contact_group->id;

        if (!empty($request->contacts) && sizeof($request->contacts) > 0) {
            foreach (array_filter($request->contacts) as $contact) {
                $n = new ContactGroupContact();
                $n->contact_group_id = $group_id;
                $n->contact_id = $contact;
                $n->save();

            }
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/tmp_numbers/';
            $fileName = 'temp_numberList' . uniqid() . '.txt';
            $file->move($destinationPath, $fileName);
            $pdo = DB::connection()->getPdo();

            $pdo->beginTransaction();
            $path = str_replace('\\', '/', $destinationPath);


            $lines = file("$path$fileName");
            $line = $lines[0];
            $uniq = rand(0, 999999999);
            switch (TRUE) {
                case substr($line, -2) === "\r\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id ,rand_id = $uniq ";
                    $pdo->exec($query);
                    break;
                case substr($line, -1) === "\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '
' IGNORE 1 LINES (first_name,last_name,number,birthday) SET business_id =  $business_id ,rand_id = $uniq";
                    $pdo->exec($query);
                    break;
                default :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id,rand_id = $uniq ";
                    $pdo->exec($query);
                    break;

            }


            $pdo->commit();

            File::delete($destinationPath . $fileName);

            $contacts = Contact::whereRandId($uniq)->get();
            foreach ($contacts as $contact) {
                $contact->rand_id = null;
                $contact->save();

                $cc = new ContactGroupContact();
                $cc->contact_group_id = $contact_group->id;
                $cc->contact_id = $contact->id;
                $cc->save();

            }

        }

        return $this->sendResponse($contact_group, 'Number Group Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\ContactGroup $contactGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactGroup $contactGroup)
    {
        $contactGroup->delete();
        return $this->sendResponse($contactGroup, 'Contact Group Deleted Successfully');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'first_name', 'last_name', 'number', 'birthday'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = ContactGroup::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = ContactGroup::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' data-toggle='tooltip' data-placement='top' title='Edit' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}' ><i class='fa fa-pencil-alt'  ></i></button>";
            }
            if ($can_delete) {
                $url = "'contact-groups/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2'  onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"><span data-toggle='tooltip' data-placement='top' title='Delete'><i class='fa fa-trash' ></i></button>";
            }

            $url = route('contact-groups.show', [$item->id]);
            $show_btn = "<a class='btn btn-xs btn-icon btn-light-success mr-2' data-toggle='tooltip' data-placement='top' title='Show' href='$url'><i class='fa fa-eye'></i></a>";

            

            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    $item->business->name,
                    $item->name,
                    $item->contacts->count(),
                    $edit_btn . $show_btn . $delete_btn
                );
            } else {
                $data[$i] = array(
                    $item->id,
                    $item->name,
                    $item->contacts->count(),
                    $edit_btn . $show_btn . $delete_btn
                );
            }

            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function numberDatatable(Request $request, ContactGroup $contactGroup)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'first_name', 'last_name', 'number', 'birthday'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = ContactGroupContact::where('contact_group_id', '=', $contactGroup->id)->tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count =ContactGroupContact::where('contact_group_id', '=', $contactGroup->id)->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-first_name='{$item->first_name}' data-last_name='{$item->last_name}' data-number='{$item->number}'' data-birthday='{$item->birthday}'><i class='fa fa-pencil-alt'  ></i></button>";
            }
            if ($can_delete) {
                $url = "'/contact-groups/delete/number/" . $item->id ."'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"><i class='fa fa-trash' ></i></button>";
            }

            $url = route('contact-groups.show', [$item->id]);


            $data[$i] = array(
                $item->id,
                $item->contactGroup->name,
                optional($item->contact)->first_name,
                optional($item->contact)->last_name,
                optional($item->contact)->number,
                optional($item->contact)->birthday,
                $delete_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function deleteFromGroup(Request $request,ContactGroupContact $contactGroupContact){
        $contactGroupContact->delete();
        return $this->sendResponse($contactGroupContact, 'Number Removed Successfully');
    }
}
