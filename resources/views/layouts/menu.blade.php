<div class="row">
    <div class="col-lg-12 d-none">
        <nav class="mobilemenu__nav">
            <ul class="meninmenu">
                <li>
                    <a href="/">Home</a>
                </li>

                <li>
                    <a href="{{route('shop')}}">Shop</a>
                </li>
                <li><a href="{{route('about')}}">About</a></li>
                <li><a href="{{route('contact')}}">Contact</a></li>
            </ul>
        </nav>
    </div>
</div>

<div class="mobile-menu d-block d-lg-none"></div>

<div class="box-menu-content block-bg block_content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2">
                <div class="pp__logo">
                    <a href="#"><img width="100px" src="images/logo/liyona.png" alt="logo"></a>
                </div>
            </div>
            <div class="col-lg-10">
                <nav class="ppnav">
                    <ul class="ppmainmenu d-flex">
                        <li class="drop"><a href="/">Home</a>
                        </li>
                        <li class="drop"><a href="{{route('shop')}}">Shop</a>
                        </li>
                        <li><a href="{{route('about')}}">About</a></li>
                        <li><a href="{{route('contact')}}">Contact</a></li>
                    </ul>
                </nav>
                <div class="close__wrap">
                    <span>close</span>
                </div>
            </div>
        </div>
    </div>
</div>
