@extends('layouts.master')
@section('title','Items')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Items List
                            <span class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} items list</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createModal">
                            New Item
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Image</th>
                            <th>Category</th>
                            <th>Brand</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="createModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">New Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'products.store', 'method' => 'post','id'=>'createForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('item.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button onclick="submitForm('createForm','createModal','datatable')"
                            type="button" class="btn btn-primary">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
    <div class="modal fade" id="editModal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Item</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {!! Form::open(['route' => 'products.store', 'method' => 'PATCH','id'=>'editForm', 'files' => true]) !!}
                <div class="modal-body">
                    @include('item.form')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="submitForm('editForm','editModal','datatable')">Save changes</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>
@endsection

@push('js')
    <script type="text/javascript">
        $(".permission_group").change(function() {
            $(".permissionss").prop("checked", this.checked);
        });
    </script>
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>
        $('#cities').select2({
            placeholder: "Select Cities",
        });

        DataTableOption.initDataTable('datatable', '/products/data/table');



        function submitForm(form_id, modal_id, table_id) {
            form_id = '#' + form_id;
            table_id = '#' + table_id;
            let url = $(form_id).attr('action');
            let method = $(form_id).attr('method');

            $(form_id).ajaxSubmit(
                {
                    clearForm: true,
                    url: url,
                    type: method,
                    success: function (result) {
                        ModalOptions.hideModal(modal_id);
                        if (result.success) {
                            let table = $(table_id).DataTable();
                            table.ajax.reload();
                            Notifications.showSuccessMsg(result.message);
                        } else {
                            Notifications.showErrorMsg(result.message);
                        }

                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        ModalOptions.hideModal(modal_id);
                        Notifications.showErrorMsg(errorThrown);
                    }
                }
            );

        }


        FormValidation.formValidation(
            document.getElementById('createForm'),
            {
                fields: {
                    name: {
                        validators: {
                            notEmpty: {
                                message: 'user Name is required'
                            },
                        }
                    },
                    permission: {
                        validators: {
                            notEmpty: {
                                message: 'Language is required'
                            },
                        }
                    },

                },

                plugins: { //Learn more: https://formvalidation.io/guide/plugins
                    trigger: new FormValidation.plugins.Trigger(),
                    // Bootstrap Framework Integration
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    // Validate fields when clicking the Submit button
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    // Submit the form when all fields are valid
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            }
        );


        function edit(item) {
            let id = item.dataset.id;
            let name = item.dataset.name;
            let description = item.dataset.description;
            let long_description = item.dataset.long_description;
            let compare_price = item.dataset.compare_price;
            let price = item.dataset.price;
            let brand_id = item.dataset.brand_id;
            let category_id = item.dataset.category_id;


            $("#editForm").find('#category_id').val(category_id);
            $("#editForm").find('#brand_id').val(brand_id);
            $("#editForm").find('#name').val(name);
            $("#editForm").find('#description').val(description);
            $("#editForm").find('#price').val(price);
            $("#editForm").find('#compare_price').val(compare_price);
            $("#editForm").find('#short_description').val(description);
            $("#editForm").find('#long_description').val(long_description);
            // $("#editForm").find('#language').trigger('change');

            $("#editForm").attr('action', '/products/' + id);
            ModalOptions.toggleModal('editModal');
        }
    </script>
@endpush
