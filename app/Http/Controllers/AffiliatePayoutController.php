<?php

namespace App\Http\Controllers;

use App\Models\Affiliate;
use App\Models\AffiliatePayout;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function Composer\Autoload\includeFile;

class AffiliatePayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('affiliate-payout.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            if (Auth::user()->business->affiliate_balance > $request->amount) {
                $payout = null;
                DB::transaction(function () use ($request) {
                    $payout = new AffiliatePayout();
                    $payout->amount = $request->amount;
                    $payout->owner_id = Auth::user()->id;
                    $payout->owner_business_id = $request->business_id;
                    $payout->requested_at = Carbon::now()->format('Y-m-d H:i:s');
                    $payout->status = 'pending';
                    $payout->save();

                });
                return $this->sendResponse($payout, 'Requested Successfully');
            }else{
                return $this->sendError('The Request amount should be less than the balance');
            }
        } catch (\Exception $exception) {
            return $this->sendError('Something went wrong Please Contact System Administrator');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AffiliatePayout  $affiliatePayout
     * @return \Illuminate\Http\Response
     */
    public function show(AffiliatePayout $affiliatePayout)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AffiliatePayout  $affiliatePayout
     * @return \Illuminate\Http\Response
     */
    public function edit(AffiliatePayout $affiliatePayout)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AffiliatePayout  $affiliatePayout
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AffiliatePayout $affiliatePayout)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\AffiliatePayout  $affiliatePayout
     * @return \Illuminate\Http\Response
     */
    public function destroy(AffiliatePayout $affiliatePayout)
    {
        //
    }
    public function datatable(Request $request)
    {
        $user = Auth::user();
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'owner_business_id', 'amount', 'status', 'requested_at'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = AffiliatePayout::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = AffiliatePayout::count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $user = Auth::user();

        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            $btnActivate = null;

            $status = $this->generateStatusLabel($item->status);


//            $upload_documents = ($item->status == 'pending') ? "<button  onclick=\"uploadDocuments(this)\" data-id='{$item->id}'  class='btn btn-xs  btn-light-warning mr-2'> <i class='fa fa-upload'></i>Upload Documents</button>" : null;
//            $make_payment = ($item->status == 'pending') ? " <button onclick=\"paymentInvoiceId(this)\" data-id='{$item->id}'  data-invoice ='{$item}'  data-user='{$item->business->owner}' class='btn btn-xs  btn-danger mr-2' > <i class='fa fa-dollar-sign'></i>Payment</button>" : null;
            if ($item->status == 'pending') {
                $btnActivate = "<button  onclick=\"activatepayoutID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Pay Now</button>";
            }


            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    optional($item->business)->name,
                    number_format($item->amount,2),
                    $status,
                    $item->requested_at,
                    $btnActivate
                );
            } else {
                $data[$i] = [
                    $item->id,
                    $item->amount,
                    $status,
                    $item->requested_at,

                ];
            }
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }
    public function generateStatusLabel($status)
    {
        $user = Auth::user();
        $label = null;
        $text = ucfirst(str_replace('_', ' ', $status));
        switch ($status) {
            case 'pending':
                $label = "<label class='label font-weight-bold label-lg  label-warning label-inline'>$text</label>";
                break;
            case 'received':
                if ($user->hasRole((['Super Admin', 'Admin']))) {
                    $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>Received</label>";
                }else{
                    $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>Paid</label>";
                }
                break;
            case 'declined':
                $label = "<label class='label font-weight-bold label-lg  label-danger label-inline'>$text</label>";
                break;
        }
        return $label;
    }
    public function activate(Request $request, AffiliatePayout $payoutsID)
    {

        if (Auth::user() && Auth::user()->can('invoice activate')) {
            $affiliatePayout = AffiliatePayout::find($request->payouts_id);
            $affiliatePayout->update([
                    'status' => 'received',
                    'paid_by' => Auth::id(),
                    'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),

                ]);

            // Mail::to($invoiceID->business->owner)->queue(new SenderIdActivated($invoiceID));
            return $this->sendResponse($affiliatePayout, 'Payout Activated Successfully!');
        } else {
            return $this->sendError('Permission Denied');
        }
    }
}
