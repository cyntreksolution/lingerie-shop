<?php

namespace App\Imports;

use App\Models\Contact;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithHeadings;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ContactsImport implements ToModel, WithHeadingRow, WithColumnFormatting
{
    protected $business_id;
    public $group_id;

    /**
     * ContactsImport constructor.
     * @param $business_id
     * @param null $group_id
     */
    public function __construct($business_id,$group_id = null)
    {
        $this->business_id = $business_id;
        $this->group_id = $group_id;
    }

    /**
     * @param array $row
     *
     * @return Contact
     */
    public function model(array $row)
    {
        return new Contact([
            'first_name' => $row['first_name'],
            'last_name' => $row['last_name'],
            'number' => $row['number'],
            'birthday' => !empty($row['birthday']) ? Date::dateTimeToExcel($row['birthday']) : null,
            'business_id' => $this->business_id,
        ]);
    }

    public function headings(): array
    {
        return 1;
    }

    public function columnFormats(): array
    {
        return [
            'D' => NumberFormat::FORMAT_DATE_YYYYMMDD,
        ];
    }


}
