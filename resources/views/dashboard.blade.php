@extends('layouts.master')
@section('title','Dashboard')
@section('content')
    <div class="container">
        <!--begin::Dashboard-->
        <!--begin::Row-->
        <div class="row">
            <div class="col-xl-12">
                <div class="row">
                    <div class="col-xl-2" >
                        <!--begin::Tiles Widget 12-->
                        <div class="card card-custom gutter-b" style="background-position: right top;
                            background-size: 30% auto;
                            background-image: url({{asset('media/misc/abstract.svg')}});
                            background-repeat: no-repeat;">
                            <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-info">
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px"
     viewBox="0 0 24 24" version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<path
                                                                                d="M4,4 L11.6314229,2.5691082 C11.8750185,2.52343403 12.1249815,2.52343403 12.3685771,2.5691082 L20,4 L20,13.2830094 C20,16.2173861 18.4883464,18.9447835 16,20.5 L12.5299989,22.6687507 C12.2057287,22.8714196 11.7942713,22.8714196 11.4700011,22.6687507 L8,20.5 C5.51165358,18.9447835 4,16.2173861 4,13.2830094 L4,4 Z"
                                                                                fill="#000000" opacity="0.3"/>
																			<path
                                                                                d="M11.1750002,14.75 C10.9354169,14.75 10.6958335,14.6541667 10.5041669,14.4625 L8.58750019,12.5458333 C8.20416686,12.1625 8.20416686,11.5875 8.58750019,11.2041667 C8.97083352,10.8208333 9.59375019,10.8208333 9.92916686,11.2041667 L11.1750002,12.45 L14.3375002,9.2875 C14.7208335,8.90416667 15.2958335,8.90416667 15.6791669,9.2875 C16.0625002,9.67083333 16.0625002,10.2458333 15.6791669,10.6291667 L11.8458335,14.4625 C11.6541669,14.6541667 11.4145835,14.75 11.1750002,14.75 Z"
                                                                                fill="#000000"/>
																		</g>
																	</svg>
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{$count['items']}}</div>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">
                                   Items
                                </a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 12-->
                    </div>
                    <div class="col-xl-2">
                        <!--begin::Tiles Widget 11-->
                        <div class="card card-custom gutter-b" >
                            <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-secondary ml-n2">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
															<rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
															<rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
															<rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
														</g>
													</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{$count['brands']}}</div>
                                <a href="#" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1"> Brands</a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 11-->
                    </div>
                    <div class="col-xl-2">
                        <!--begin::Tiles Widget 12-->
                        <div class="card card-custom gutter-b" style="height: 150px">
                            <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-warning">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="0" y="0" width="24" height="24"></rect>
															<path d="M16,15.6315789 L16,12 C16,10.3431458 14.6568542,9 13,9 L6.16183229,9 L6.16183229,5.52631579 C6.16183229,4.13107011 7.29290239,3 8.68814808,3 L20.4776218,3 C21.8728674,3 23.0039375,4.13107011 23.0039375,5.52631579 L23.0039375,13.1052632 L23.0206157,17.786793 C23.0215995,18.0629336 22.7985408,18.2875874 22.5224001,18.2885711 C22.3891754,18.2890457 22.2612702,18.2363324 22.1670655,18.1421277 L19.6565168,15.6315789 L16,15.6315789 Z" fill="#000000"></path>
															<path d="M1.98505595,18 L1.98505595,13 C1.98505595,11.8954305 2.88048645,11 3.98505595,11 L11.9850559,11 C13.0896254,11 13.9850559,11.8954305 13.9850559,13 L13.9850559,18 C13.9850559,19.1045695 13.0896254,20 11.9850559,20 L4.10078614,20 L2.85693427,21.1905292 C2.65744295,21.3814685 2.34093638,21.3745358 2.14999706,21.1750444 C2.06092565,21.0819836 2.01120804,20.958136 2.01120804,20.8293182 L2.01120804,18.32426 C1.99400175,18.2187196 1.98505595,18.1104045 1.98505595,18 Z M6.5,14 C6.22385763,14 6,14.2238576 6,14.5 C6,14.7761424 6.22385763,15 6.5,15 L11.5,15 C11.7761424,15 12,14.7761424 12,14.5 C12,14.2238576 11.7761424,14 11.5,14 L6.5,14 Z M9.5,16 C9.22385763,16 9,16.2238576 9,16.5 C9,16.7761424 9.22385763,17 9.5,17 L11.5,17 C11.7761424,17 12,16.7761424 12,16.5 C12,16.2238576 11.7761424,16 11.5,16 L9.5,16 Z" fill="#000000" opacity="0.3"></path>
														</g>
													</svg>
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{$count['category']}}</div>
                                <a href="#"
                                   class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1"> Categories</a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 12-->
                    </div>
                    <div class="col-xl-2">
                        <!--begin::Tiles Widget 11-->
                        <div class="card card-custom  gutter-b" style="height: 150px">
                            <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-danger ml-n2">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Layout/Layout-4-blocks.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<rect x="0" y="0" width="24" height="24"/>
																			<rect fill="#000000" x="4" y="4" width="7"
                                                                                  height="7" rx="1.5"/>
																			<path
                                                                                d="M5.5,13 L9.5,13 C10.3284271,13 11,13.6715729 11,14.5 L11,18.5 C11,19.3284271 10.3284271,20 9.5,20 L5.5,20 C4.67157288,20 4,19.3284271 4,18.5 L4,14.5 C4,13.6715729 4.67157288,13 5.5,13 Z M14.5,4 L18.5,4 C19.3284271,4 20,4.67157288 20,5.5 L20,9.5 C20,10.3284271 19.3284271,11 18.5,11 L14.5,11 C13.6715729,11 13,10.3284271 13,9.5 L13,5.5 C13,4.67157288 13.6715729,4 14.5,4 Z M14.5,13 L18.5,13 C19.3284271,13 20,13.6715729 20,14.5 L20,18.5 C20,19.3284271 19.3284271,20 18.5,20 L14.5,20 C13.6715729,20 13,19.3284271 13,18.5 L13,14.5 C13,13.6715729 13.6715729,13 14.5,13 Z"
                                                                                fill="#000000" opacity="0.3"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{0}}</div>
                                <a href="" class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">Orders</a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 11-->
                    </div>
                    <div class="col-xl-2">
                        <!--begin::Tiles Widget 12-->
                        <div class="card card-custom gutter-b" style="height: 150px">
                            <div class="card-body">
                            <a href="">
																<span class="svg-icon svg-icon-3x svg-icon-success">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24"/>
																			<path
                                                                                d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
																			<path
                                                                                d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{1}}</div>
                                <a href=""
                                   class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">Users</a>
                            </a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 12-->
                    </div>
                    <div class="col-xl-2">
                        <!--begin::Tiles Widget 12-->
                        <div class="card card-custom gutter-b" style="height: 150px">
                            <div class="card-body">
																<span class="svg-icon svg-icon-3x svg-icon-primary">
																	<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
																	<svg xmlns="http://www.w3.org/2000/svg"
                                                                         xmlns:xlink="http://www.w3.org/1999/xlink"
                                                                         width="24px" height="24px" viewBox="0 0 24 24"
                                                                         version="1.1">
																		<g stroke="none" stroke-width="1" fill="none"
                                                                           fill-rule="evenodd">
																			<polygon points="0 0 24 0 24 24 0 24"/>
																			<path
                                                                                d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z"
                                                                                fill="#000000" fill-rule="nonzero"
                                                                                opacity="0.3"/>
																			<path
                                                                                d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z"
                                                                                fill="#000000" fill-rule="nonzero"/>
																		</g>
																	</svg>
                                                                    <!--end::Svg Icon-->
																</span>
                                <div
                                    class="text-dark font-weight-bolder font-size-h2 mt-3">{{1}}</div>
                                <a

                                   class="text-muted text-hover-primary font-weight-bold font-size-lg mt-1">New Users</a>
                            </div>
                        </div>
                        <!--end::Tiles Widget 12-->
                    </div>

                </div>
                <div class="row">
                    <div class="col-xl-12">
                        <div class="card card-custom gutter-b">
                            <!--begin::Header-->
                            <div class="card-header h-auto">
                                <!--begin::Title-->
                                <div class="card-title py-5">
                                    <h3 class="card-label">Past 30 Days Sales</h3>
                                </div>
                                <!--end::Title-->
                            </div>
                            <div class="card-body" style="position: relative;">
                                <div id="chart" style="height: 250px;"></div>
                            </div>
                        </div>
                        <div class="col-xl-6">

                        </div>
                    </div>
                </div>
            </div>

        </div>
        @endsection

        @push('js')
            <script>
                var options = {
                    chart: {
                        type: 'area',
                        height: '250'
                    },
                    stroke: {
                        curve: 'smooth',
                    },
                    series: [{
                        name: 'Sales',
                        data: [1000.00,2400.00,2051.00,4012.00,3052.00,1042.00,4506.00]
                    }],
                    xaxis: {
                        categories:['01-02-2021','02-02-2021','03-02-2021','04-02-2021','05-02-2021','06-02-2021','07-02-2021']
                    }
                }

                var chart = new ApexCharts(document.querySelector("#chart"), options);

                chart.render();
            </script>
    @endpush
