@extends('layouts.front')

@section('content')
    <div class="ht__bradcaump__area bg-image--68">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="bradcaump__inner text-center">
                        <h2 class="bradcaump-title">Shop Single</h2>
                        <nav class="bradcaump-content">
                            <a class="breadcrumb_item" href="/">Home</a>
                            <span class="brd-separetor">/</span>
                            <span class="breadcrumb_item active">{{$product->name}}</span>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="maincontent bg--white section-padding--lg">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-12">

                    <div class="wn__single__product">
                        <div class="row">
                            <div class="col-lg-6 col-12">
                                <div class="wn__fotorama__wrapper">
                                    <div class="fotorama wn__fotorama__action" data-nav="thumbs">
                                        <a href=""><img src="{{asset('items/'.$product->featured_image)}}" alt=""></a>
{{--                                        <a href="2.jpg"><img src="images/product/2.jpg" alt=""></a>--}}
{{--                                        <a href="3.jpg"><img src="images/product/3.jpg" alt=""></a>--}}
{{--                                        <a href="4.jpg"><img src="images/product/4.jpg" alt=""></a>--}}
{{--                                        <a href="5.jpg"><img src="images/product/5.jpg" alt=""></a>--}}
{{--                                        <a href="6.jpg"><img src="images/product/6.jpg" alt=""></a>--}}
{{--                                        <a href="7.jpg"><img src="images/product/7.jpg" alt=""></a>--}}
{{--                                        <a href="8.jpg"><img src="images/product/8.jpg" alt=""></a>--}}
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-12">
                                <div class="product__info__main">
                                    <h1>{{$product->name}}</h1>
                                    <div class="product-info-stock-sku d-flex">
                                        <p>Availability:<span> In stock</span></p>
                                        {{--                                        <p>SKU:<span> MH01</span></p>--}}
                                    </div>
                                    {{--                                    <div class="product-reviews-summary d-flex">--}}
                                    {{--                                        <ul class="rating-summary d-flex">--}}
                                    {{--                                            <li><i class="zmdi zmdi-star-outline"></i></li>--}}
                                    {{--                                            <li><i class="zmdi zmdi-star-outline"></i></li>--}}
                                    {{--                                            <li><i class="zmdi zmdi-star-outline"></i></li>--}}
                                    {{--                                            <li class="off"><i class="zmdi zmdi-star-outline"></i></li>--}}
                                    {{--                                            <li class="off"><i class="zmdi zmdi-star-outline"></i></li>--}}
                                    {{--                                        </ul>--}}
                                    {{--                                        <div class="reviews-actions d-flex">--}}
                                    {{--                                            <a href="#">(1 Review)</a>--}}
                                    {{--                                            <a href="#">Add Your Review</a>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    <div class="price-box">
                                        <span>{{$product->price}} LKR</span>
                                    </div>
                                    {{--                                    <div class="product-color-label">--}}
                                    {{--                                        <span>Color</span>--}}
                                    {{--                                        <div class="color__attribute d-flex">--}}
                                    {{--                                            <div class="swatch-option color" style="background: #000000 no-repeat center; background-size: initial;"></div>--}}

                                    {{--                                            <div class="swatch-option color" style="background: #8f8f8f no-repeat center; background-size: initial;"></div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="box-tocart d-flex">--}}
                                    {{--                                        <span>Qty</span>--}}
                                    {{--                                        <input id="qty" class="input-text qty" name="qty" min="1" value="1" title="Qty" type="number">--}}

                                    {{--                                        <div class="addtocart__actions">--}}
                                    {{--                                            <button  class="tocart" type="submit" title="Add to Cart">Add to Cart</button>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="product-addto-links clearfix">--}}
                                    {{--                                        <a class="wishlist" href="#"></a>--}}
                                    {{--                                        <a class="compare" href="#"></a>--}}
                                    {{--                                        <a class="email" href="#"></a>--}}
                                    {{--                                    </div>--}}
                                    <div class="product__overview">
                                        {!! $product->description !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="product__info__detailed">
                        <div class="pro_details_nav nav justify-content-start" role="tablist">
                            <a class="nav-item nav-link active" data-toggle="tab" href="#nav-details" role="tab">Details</a>
                            {{--                            <a class="nav-item nav-link" data-toggle="tab" href="#nav-review" role="tab">Reviews</a>--}}
                        </div>
                        <div class="tab__container">
                            <!-- Start Single Tab Content -->
                            <div class="pro__tab_label tab-pane fade show active" id="nav-details" role="tabpanel">
                                <div class="description__attribute">
                                    {!! $product->long_description !!}
                                </div>
                            </div>
                            <!-- End Single Tab Content -->
                            <!-- Start Single Tab Content -->
                        {{--                            <div class="pro__tab_label tab-pane fade" id="nav-review" role="tabpanel">--}}
                        {{--                                <div class="review__attribute">--}}
                        {{--                                    <h1>Customer Reviews</h1>--}}
                        {{--                                    <h2>Hastech</h2>--}}
                        {{--                                    <div class="review__ratings__type d-flex">--}}
                        {{--                                        <div class="review-ratings">--}}
                        {{--                                            <div class="rating-summary d-flex">--}}
                        {{--                                                <span>Quality</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}

                        {{--                                            <div class="rating-summary d-flex">--}}
                        {{--                                                <span>Price</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}

                        {{--                                            <div class="rating-summary d-flex">--}}
                        {{--                                                <span>value</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="review-content">--}}
                        {{--                                            <p>Hastech</p>--}}
                        {{--                                            <p>Review by Hastech</p>--}}
                        {{--                                            <p>Posted on 7/3/2018</p>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}

                        {{--                                <div class="review-fieldset">--}}
                        {{--                                    <h2>You're reviewing:</h2>--}}
                        {{--                                    <h3>Chaz Kangeroo Hoodie</h3>--}}

                        {{--                                    <div class="review-field-ratings">--}}
                        {{--                                        <div class="product-review-table">--}}

                        {{--                                            <div class="review-field-rating d-flex">--}}
                        {{--                                                <span>Quality</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}

                        {{--                                            <div class="review-field-rating d-flex">--}}
                        {{--                                                <span>Price</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}

                        {{--                                            <div class="review-field-rating d-flex">--}}
                        {{--                                                <span>Value</span>--}}
                        {{--                                                <ul class="rating d-flex">--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                    <li class="off"><i class="zmdi zmdi-star"></i></li>--}}
                        {{--                                                </ul>--}}
                        {{--                                            </div>--}}

                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                    <div class="review_form_field">--}}
                        {{--                                        <div class="input__box">--}}
                        {{--                                            <span>Nickname</span>--}}
                        {{--                                            <input id="nickname_field" type="text" name="nickname">--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="input__box">--}}
                        {{--                                            <span>Summary</span>--}}
                        {{--                                            <input id="summery_field" type="text" name="summery">--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="input__box">--}}
                        {{--                                            <span>Review</span>--}}
                        {{--                                            <textarea name="review"></textarea>--}}
                        {{--                                        </div>--}}
                        {{--                                        <div class="review-form-actions">--}}
                        {{--                                            <button>Submit Review</button>--}}
                        {{--                                        </div>--}}
                        {{--                                    </div>--}}
                        {{--                                </div>--}}


                        {{--                            </div>--}}
                        <!-- End Single Tab Content -->
                        </div>
                    </div>
                    <div class="wn__related__product ptb--100">
                        <div class="section__title text-center">
                            <h2 class="title__be--2">Related Products</h2>
                        </div>
                        <div class="row mt--60">
                            <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                                @php $category = \App\Models\Category::find($product->category_id) @endphp
                                @foreach($category->products as $product)
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                        <div class="product">
                                            <div class="product__thumb">
                                                <a class="first__img" href="{{route('product',[$product->id])}}"><img
                                                        src="{{asset('items/'.$product->featured_image)}}"
                                                        alt="product image"></a>
                                                <a class="second__img animation1"
                                                   href="{{route('product',[$product->id])}}"><img
                                                        src="{{asset('items/'.$product->featured_image)}}"
                                                        alt="product image"></a>

                                                <div class="new__box">
                                                    <span class="new-label">New</span>
                                                </div>

                                                <ul class="prize position__right__bottom d-flex">
                                                    <li>{{$product->price}} LKR</li>
                                                    @if(!empty($product->compare_price))
                                                        <li class="old_prize">{{$product->compare_price}} LKR</li>
                                                    @endif
                                                </ul>

                                                {{--                                            <div class="action">--}}
                                                {{--                                                <div class="actions_inner">--}}
                                                {{--                                                    <ul class="add_to_links">--}}
                                                {{--                                                <li><a class="cart" href="#"></a></li>--}}
                                                {{--                                                <li><a class="wishlist" href="#"></a></li>--}}
                                                {{--                                                <li><a class="compare" href="#"></a></li>--}}
                                                {{--                                                        <li><a data-toggle="modal" title="Quick View"--}}
                                                {{--                                                               class="quickview modal-view detail-link"--}}
                                                {{--                                                               href="#productmodal{{$product->id}}"></a></li>--}}
                                                {{--                                                    </ul>--}}
                                                {{--                                                </div>--}}
                                                {{--                                            </div>--}}

                                            </div>
                                            <div class="product__content">
                                                <h4><a href="{{route('product',[$product->id])}}">{{$product->name}}</a></h4>
                                                {{--                                            <ul class="rating d-flex">--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                            </ul>--}}
                                            </div>
                                        </div>
                                    </div>


                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="wn__related__product">
                        <div class="section__title text-center">
                            <h2 class="title__be--2">upsell products</h2>
                        </div>
                        <div class="row mt--60">
                            <div class="productcategory__slide--2 arrows_style owl-carousel owl-theme">
                                @php $category = \App\Models\Category::find($product->category_id) @endphp
                                @foreach($category->products as $product)
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12">
                                        <div class="product">
                                            <div class="product__thumb">
                                                <a class="first__img" href="{{route('product',[$product->id])}}"><img
                                                        src="{{asset('items/'.$product->featured_image)}}"
                                                        alt="product image"></a>
                                                <a class="second__img animation1"
                                                   href="{{route('product',[$product->id])}}"><img
                                                        src="{{asset('items/'.$product->featured_image)}}"
                                                        alt="product image"></a>

                                                <div class="new__box">
                                                    <span class="new-label">New</span>
                                                </div>

                                                <ul class="prize position__right__bottom d-flex">
                                                    <li>{{$product->price}} LKR</li>
                                                    @if(!empty($product->compare_price))
                                                        <li class="old_prize">{{$product->compare_price}} LKR</li>
                                                    @endif
                                                </ul>

                                                {{--                                            <div class="action">--}}
                                                {{--                                                <div class="actions_inner">--}}
                                                {{--                                                    <ul class="add_to_links">--}}
                                                {{--                                                <li><a class="cart" href="#"></a></li>--}}
                                                {{--                                                <li><a class="wishlist" href="#"></a></li>--}}
                                                {{--                                                <li><a class="compare" href="#"></a></li>--}}
                                                {{--                                                        <li><a data-toggle="modal" title="Quick View"--}}
                                                {{--                                                               class="quickview modal-view detail-link"--}}
                                                {{--                                                               href="#productmodal{{$product->id}}"></a></li>--}}
                                                {{--                                                    </ul>--}}
                                                {{--                                                </div>--}}
                                                {{--                                            </div>--}}

                                            </div>
                                            <div class="product__content">
                                                <h4><a href="{{route('product',[$product->id])}}">{{$product->name}}</a></h4>
                                                {{--                                            <ul class="rating d-flex">--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li class="on"><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                                <li><i class="fa fa-star-o"></i></li>--}}
                                                {{--                                            </ul>--}}
                                            </div>
                                        </div>
                                    </div>


                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-12 md-mt-40 sm-mt-40">
                    <div class="shop__sidebar">
                        <aside class="wedget__categories poroduct--cat">
                            <h3 class="wedget__title">Product Categories</h3>
                            <ul>
                                @foreach($categories as $category)
                                    <li><a href="#">{{$category->name}} </a></li>
{{--                                    <li><a href="#">{{$category->name}} <span>(3)</span></a></li>--}}
                                @endforeach
                            </ul>
                        </aside>

                        {{--                        <aside class="wedget__categories pro--range">--}}
                        {{--                            <h3 class="wedget__title">Filter by price</h3>--}}
                        {{--                            <div class="content-shopby">--}}
                        {{--                                <div class="price_filter s-filter clear">--}}
                        {{--                                    <form action="#" method="GET">--}}
                        {{--                                        <div id="slider-range"></div>--}}
                        {{--                                        <div class="slider__range--output">--}}
                        {{--                                            <div class="price__output--wrap">--}}
                        {{--                                                <div class="price--output">--}}
                        {{--                                                    <span>Price :</span><input type="text" id="amount" readonly>--}}
                        {{--                                                </div>--}}
                        {{--                                                <div class="price--filter">--}}
                        {{--                                                    <a href="#">Filter</a>--}}
                        {{--                                                </div>--}}
                        {{--                                            </div>--}}
                        {{--                                        </div>--}}
                        {{--                                    </form>--}}
                        {{--                                </div>--}}
                        {{--                            </div>--}}
                        {{--                        </aside>--}}

                        {{--                        <aside class="wedget__categories poroduct--compare">--}}
                        {{--                            <h3 class="wedget__title">Compare</h3>--}}
                        {{--                            <ul>--}}
                        {{--                                <li><a href="#">x</a><a href="#">Condimentum posuere</a></li>--}}
                        {{--                                <li><a href="#">x</a><a href="#">Condimentum posuere</a></li>--}}
                        {{--                                <li><a href="#">x</a><a href="#">Dignissim venenatis</a></li>--}}
                        {{--                            </ul>--}}
                        {{--                        </aside>--}}

                        {{--                        <aside class="wedget__categories poroduct--tag">--}}
                        {{--                            <h3 class="wedget__title">Product Tags</h3>--}}
                        {{--                            <ul>--}}
                        {{--                                <li><a href="#">blouse</a></li>--}}
                        {{--                                <li><a href="#">clothes</a></li>--}}
                        {{--                                <li><a href="#">fashion</a></li>--}}
                        {{--                                <li><a href="#">handbag</a></li>--}}
                        {{--                                <li><a href="#">fashion</a></li>--}}
                        {{--                                <li><a href="#">handbag</a></li>--}}
                        {{--                                <li><a href="#">handbag</a></li>--}}
                        {{--                                <li><a href="#">fashion</a></li>--}}
                        {{--                                <li><a href="#">handbag</a></li>--}}
                        {{--                                <li><a href="#">hat</a></li>--}}
                        {{--                                <li><a href="#">laptop</a></li>--}}
                        {{--                                <li><a href="#">hat</a></li>--}}
                        {{--                                <li><a href="#">laptop</a></li>--}}
                        {{--                            </ul>--}}
                        {{--                        </aside>--}}

                        <aside class="wedget__categories sidebar--banner">
                            <img src="{{asset('images/others/banner_left.jpg')}}" alt="banner images">
                            <div class="text">
                                <h2>new products</h2>
                                <h6>save up to <br> <strong>40%</strong>off</h6>
                            </div>
                        </aside>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wn__brand__area bg__cat--1 ptb--120">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="brand__activation arrows_style owl-carousel owl-theme">
                        <li><a href="#"><img src="images/brand/1.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/2.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/3.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/4.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/5.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/6.png" alt="brand images"></a></li>
                        <li><a href="#"><img src="images/brand/7.png" alt="brand images"></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section class="wn__newsletter__area bg--white">
        <div class="container">
            <div class="row newsletter--bg">
                <div class="col-lg-7 offset-lg-5 col-md-12 col-12">
                    <div class="section__title text-center">
                        <h2>Sign up to newsletter</h2>
                    </div>
                    <div class="newsletter__block">
                        <p>Subscribe to our newsletters now and stay up-to-date with new collections, the latest lookbooks and exclusive offers.</p>
                        <form action="#">
                            <div class="newsletter__box">
                                <input type="email" placeholder="Enter your e-mail">
                                <button>Subscribe</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('styles')

@endpush
@push('scripts')

@endpush
