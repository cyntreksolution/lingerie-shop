@extends('layouts.master')
@section('title','Instant SMS List')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Instant SMS List
                            <span class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} Instant SMS  list</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{route('sms.index')}}" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Create New Instant SMS">
                            <i class="fa fa-mail-bulk mr-1"></i> New Instant SMS
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Sender ID</th>
                            <th>Number</th>
                            <th>Message Content</th>
                            <th>Status</th>

{{--                            <th>Instant / Scheduled</th>--}}
{{--                            <th>Date & Time</th>--}}
                            <th>Charge</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>
        DataTableOption.initDataTable('datatable', '/sms/data/table',true,[0,'desc']);

        function submitForm(form_id, modal_id, table_id) {
            validateFormNow(form_id).validate().then(function (status) {
                if (status == "Valid") {
                    form_id = '#' + form_id;
                    table_id = '#' + table_id;
                    let url = $(form_id).attr('action');
                    let method = $(form_id).attr('method');

                    $(form_id).ajaxSubmit(
                        {
                            clearForm: true,
                            url: url,
                            type: method,
                            success: function (result) {
                                ModalOptions.hideModal(modal_id);
                                if (result.success) {
                                    let table = $(table_id).DataTable();
                                    table.ajax.reload();
                                    Notifications.showSuccessMsg(result.message);
                                } else {
                                    Notifications.showErrorMsg(result.message);
                                }

                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                ModalOptions.hideModal(modal_id);
                                Notifications.showErrorMsg(errorThrown);
                            }
                        }
                    );
                }
            });
        }



    </script>
@endpush
