<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">

            @foreach($permissionGroups as $value)
                <ol>
                    <li style="list-style-type: none; ">
                <label style="font-weight: bold;color: #0a6aa1">{{ Form::checkbox('permission_group[]', $value->id, array('class' => 'name permission_group'.$value->id,'id' => 'permission_group')) }}
                    {{ $value->name }}</label>
                <br/>
                    </li>
                    <ul><li style="list-style-type: none;">
                @foreach($value->permissions as $permission)
                    <label>{{ Form::checkbox('permission[]', $permission->id, array('class' => 'name','id' => 'permission')) }}
                        {{ $permission->name }}</label>
                    <br/>
                @endforeach
                        </li></ul>
                </ol>
            @endforeach

        </div>
    </div>
</div>



