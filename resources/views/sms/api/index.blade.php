@extends('layouts.master')
@section('title','API SMS List')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">

            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">API SMS List
                            <span class="d-block text-muted pt-2 font-size-sm">{{env('APP_NAME')}} API SMS  list</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <table class="table table-bordered table-checkable" id="datatable">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Sender ID</th>
                            <th>Message Content</th>
                            <th>Status</th>
{{--                            <th>Instant / Scheduled</th>--}}
{{--                            <th>Date & Time</th>--}}
                            <th>Charge</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>

    </div>

@endsection

@push('js')
    <script src="{{asset('js/jquery.form.js')}}"></script>
    <script>
        DataTableOption.initDataTable('datatable', '/api/data/table',true,[0,'desc']);
    </script>
@endpush
