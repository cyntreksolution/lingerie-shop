<?php

namespace App\Http\Controllers;

use App\Events\BulkCampaignCreated;
use App\Events\SendBulkSMS;
use App\Models\Campaign;
use App\Models\CampaignContact;
use App\Models\CampaignGroup;
use App\Models\CampaignNumber;
use App\Models\Contact;
use App\Models\ContactGroup;
use App\Models\ScheduledCampaign;
use App\Models\SenderID;
use App\Traits\SMSPriceTrait;
use App\Traits\SMSTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CampaignController extends Controller
{
    use SMSTrait;
    use SMSPriceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Htctp\Response
     */
    public function index()
    {
        if (Auth::user()->isDemoUser()) {
            $sender_ids = SenderID::withoutGlobalScopes(['company'])->where('sender_id', '=', 'KentacyDEMO')->pluck('sender_id', 'id');
        } else {
            $sender_ids = SenderID::where('status', '=', 'active')->pluck('sender_id', 'id');
        }

        $business_id = Auth::user()->business_id;
        $contacts = Contact::whereBusinessId($business_id)->pluck('first_name', 'id');
        $groups = ContactGroup::whereBusinessId($business_id)->pluck('name', 'id');

        if (sizeof($contacts) > 1) {
            $contacts->prepend('ALL', 0);
        }
        if (sizeof($groups) > 1) {
            $groups->prepend('ALL', 0);
        }


        return view('sms.campaign', compact('sender_ids', 'contacts', 'groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $name = $request->name;
        $numbers = $request->numbers;
        $contacts = $request->contacts;
        $groups = $request->groups;
        $is_schedule = $request->is_schedule;
        $schedule_time = $request->schedule_time;
        $sender_id = $request->sender_id;
        $message = $request->message;
        $business = Auth::user()->business;

        if ($is_schedule) {
            if (Carbon::parse($schedule_time)->format('Y-m-d H:i') <= Carbon::now()->format('Y-m-d H:i')) {
                return $this->sendError('Invalid Schedule Time');
            }
        }

        $campaign = new Campaign();
        $campaign->business_id = $business->id;
        $campaign->user_id = Auth::id();
        $campaign->name = $name;
        $campaign->sender_id = $sender_id;
        $campaign->type = 'campaign';
        $campaign->message = $message;
        $campaign->is_scheduled = !empty($is_schedule) ? 1 : 0;
        $campaign->scheduled_time = !empty($is_schedule) ? Carbon::parse($schedule_time)->format('Y-m-d H:i') : null;
        $campaign->status = !empty($is_schedule) ? 'pending' : 'running';
        $campaign->sms_count = 0;
        $campaign->page_count = $this->count($message)->messages;
        $campaign->cost_per_sms = $business->sms_charge;
        $campaign->start_balance = 0;
        $campaign->sms_charge = 0;
        $campaign->end_balance = 0;

        $campaign->save();


        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/campaign_numbers/';
            $fileName = 'campaign_numberList' . uniqid() . '.txt';
            $file->move($destinationPath, $fileName);

            $campaign->csv_file = $destinationPath . $fileName;

            $pdo = DB::connection()->getPdo();

            $pdo->beginTransaction();
            $path = str_replace('\\', '/', $destinationPath);


            $lines = file("$path$fileName");
            $line = $lines[0];
            switch (TRUE) {
                case substr($line, -2) === "\r\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE campaign_numbers FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(@number) SET campaign_id =  $campaign->id , business_id =  $business->id ,csv_file='$fileName', number = TRIM(@number) ";
                    $pdo->exec($query);
                    break;
                case substr($line, -1) === "\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE campaign_numbers FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '
' IGNORE 1 LINES (@number) SET campaign_id =  $campaign->id , business_id =  $business->id ,csv_file='$fileName',number = TRIM(@number)";
                    $pdo->exec($query);
                    break;
                default :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE campaign_numbers FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(@number) SET campaign_id =  $campaign->id , business_id =  $business->id ,csv_file='$fileName', number = TRIM(@number)";
                    $pdo->exec($query);
                    break;

            }

            $pdo->commit();
        }


        if (!empty($numbers)) {
            $numbers = collect(json_decode($numbers))->pluck('value');
            foreach ($numbers as $number) {
                $cn = CampaignNumber::firstOrCreate(['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $number]);
            }
        }

        if (!empty($contacts)) {
            $all_selected = collect($contacts)->contains(0);

            if ($all_selected) {
                $contacts = Contact::whereBusinessId($business->id)->get();
            } else {
                $contacts = Contact::whereIn('id', $contacts)->get();
            }


            foreach ($contacts as $number) {
                $contact = new CampaignContact();
                $contact->business_id = $business->id;
                $contact->campaign_id = $campaign->id;
                $contact->contact_id = $number->id;
                $contact->save();
                $cn = CampaignNumber::firstOrCreate(
                    ['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $number->number],
                    ['contact_id' => $number->id]);
            }
        }

        if (!empty($groups)) {

            $all_selected = collect($groups)->contains(0);
            if ($all_selected) {
                $groups = ContactGroup::whereBusinessId($business->id)->get();
            } else {
                $groups = ContactGroup::whereIn('id', $groups)->get();
            }


            foreach ($groups as $group) {
                foreach ($group->contacts as $number) {
                    $cn = CampaignNumber::firstOrCreate(
                        ['business_id' => $business->id, 'campaign_id' => $campaign->id, 'number' => $number->number],
                        ['contact_group_id' => $group->id]);
                }

                $cgroup = new CampaignGroup();
                $cgroup->business_id = $business->id;
                $cgroup->campaign_id = $campaign->id;
                $cgroup->contact_group_id = $group->id;
                $cgroup->save();
            }
        }

        $number_count = CampaignNumber::whereCampaignId($campaign->id)->count();
        $campaign->sms_count = $number_count;

        $charge = ($campaign->sms_count * $campaign->page_count) * $campaign->cost_per_sms;
        $real_cost = ($campaign->sms_count * $campaign->page_count) * 0.62;
        $profit = $charge - $real_cost;

        $campaign->profit = $profit;
        $campaign->sms_charge = $charge;
        $campaign->save();


        if ($this->hasActiveSubscription($business)) {
            $available_qty = $this->availableSMSQty($business);
            $campaign_sms_count = $campaign->sms_count * $campaign->page_count;
            if ($available_qty >= $campaign_sms_count) {
                if (!$campaign->is_scheduled) {
                    $created = BulkCampaignCreated::dispatch($campaign);
                } else {
                    $this->scheduleCampaign($campaign);
                }
            } else {
                return $this->sendError('Your Account Balance Is Insufficient . Please Top UP');
            }

        } elseif ($campaign->sms_charge < $business->balance) {
            $charge = ($campaign->sms_count * $campaign->page_count) * $campaign->cost_per_sms;
            $real_cost = ($campaign->sms_count * $campaign->page_count) * 0.62;
            $profit = $charge - $real_cost;

            $campaign->profit = $profit;
            $campaign->sms_charge = $charge;
            $campaign->save();
            if (!$campaign->is_scheduled) {
                $created = BulkCampaignCreated::dispatch($campaign);
            } else {
                $this->scheduleCampaign($campaign);
            }
        } else {
            $campaign->status = 'failed';
            $campaign->error_code = 'insufficient_balance';
            $campaign->save();
            return $this->sendError('Your Account Balance Is Insufficient . Please Top UP');
        }

        return $this->sendResponse($campaign, 'Message Sent Successfully');
    }

    function scheduleCampaign($campaign)
    {
        $sc = new ScheduledCampaign();
        $sc->campaign_id = $campaign->id;
        $sc->scheduled_at = Carbon::parse($campaign->scheduled_time)->format('Y-m-d H:i');
        $sc->save();
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\Response
     */
    public function show(Campaign $campaign, $id)
    {

        $campaignId = $id;

        $chartData = CampaignNumber::whereCampaignId($id)->get();

        $pendingCount = $chartData->where('sms_status', 'pending')->count();
        $sentCount = $chartData->where('sms_status', 'sent')->count();
        $deliveryCount = $chartData->where('sms_status', 'delivered')->count();
        $failedCount = $chartData->where('sms_status', 'failed')->count();


        $chartDataArray = [$pendingCount, $sentCount, $deliveryCount, $failedCount];

        return view('sms.campaign.campaing-numbers', compact('campaignId', 'chartDataArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\Response
     */
    public function edit(Campaign $campaign)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Campaign $campaign)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\Response
     */
    public function destroy(Campaign $campaign)
    {
        //
    }


    public function listView()
    {
        return view('sms.campaign.index');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['campaigns.created_at', 'name', 'sender_ids.sender_id', 'message', 'campaigns.status', 'is_scheduled', 'scheduled_time', 'sms_charge'];

        $order_column = $columns[$order_by[0]['column']];
        $dataset = Campaign::whereType('campaign')->tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Campaign::whereType('campaign')->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;


        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;
            $campaigns_show_url = route('sms-campaigns.show', [$item->id]);
            if ($can_edit) {
//                if ($item->is_scheduled && $item->status == 'pending') {
//                    $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}'><i class='fa fa-pencil-alt'  ></i></button>";
//                }
            }
            if ($can_delete) {
                $url = "'contacts/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"><i class='fa fa-trash' ></i></button>";
            }

            $show_btn = "<a  target='_blank' href='$campaigns_show_url'  class='btn btn-xs btn-icon btn-light-success mr-2'><i class='fa fa-eye'></i></button>";


            $scheduled = ($item->is_scheduled) ? "<label class='label label-lg font-weight-bold label-light-dark label-inline'>Scheduled</label>" : "<label class='label label-lg font-weight-bold label-light-primary label-inline'>Instant</label>";

            $status = null;

            switch ($item->status) {
                case 'pending':
                    $status = "<label class='label label-lg font-weight-bold label-light-warning label-inline'>Pending</label>";
                    break;
                case 'running':
                    $status = "<label class='label label-lg font-weight-bold label-light-info label-inline'>Running</label>";
                    break;
                case 'sent':
                    $status = "<label class='label label-lg font-weight-bold label-light-success label-inline'>Sent</label>";
                    break;
                case 'cancelled':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Cancelled</label>";
                    break;
                case 'failed':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Failed</label>";
                    break;
            }

            $data[$i] = array(
                Carbon::parse($item->created_at)->format('Y-m-d H:i:s'),
                $item->name,
                $item->senderID->sender_id,
                $item->message,
                $status,
                $scheduled,
                $item->scheduled_time,
                $item->page_count,
                $item->sms_count,
                number_format($item->sms_charge, 2) . ' LKR',
                $edit_btn . $show_btn
            );
            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function datatable_numbers(Request $request, $id)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['created_at', 'number', 'sms_status', 'delivery_time', 'error_text'];

        $order_column = $columns[$order_by[0]['column']];
        $query = CampaignNumber::query();


        $filterCount = $totalCount = CampaignNumber::where('campaign_id', '=', $id)->count();

        if ($request->filter) {
            $query = $query->filterData($request->delivered_at, $request->status)->where('campaign_id', '=', $id);
            $filterCount = $query->count();
        } elseif (is_null($search) || empty($search)) {
            $query = $query->where('campaign_id', '=', $id);
        } else {
            $query = $query->searchData($search)->where('campaign_id', '=', $id);
            $filterCount = $query->count();
        }
        $query = $query->tableData($order_column, $order_by_str, $start, $length)->get();


        $data = [];
        $i = 0;


        foreach ($query as $key => $item) {
            $status = null;

            switch ($item->sms_status) {
                case 'pending':
                    $status = "<label class='label label-lg font-weight-bold label-light-warning label-inline'>Pending</label>";
                    break;
                case 'sent':
                    $status = "<label class='label label-lg font-weight-bold label-light-info label-inline'>Sent</label>";
                    break;
                case 'delivered':
                    $status = "<label class='label label-lg font-weight-bold label-light-success label-inline'>Delivered</label>";
                    break;
                case 'failed':
                    $status = "<label class='label label-lg font-weight-bold label-light-danger label-inline'>Failed</label>";
                    break;
            }

            $data[$i] = array(
                Carbon::parse($item->created_at)->format('Y-m-d h:s:i'),
                $item->number,
                $status,
                $item->delivery_time,
                $item->error_text,

            );
            $i++;
        }


        if ($totalCount == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($totalCount),
            "recordsFiltered" => intval($filterCount),
            "data" => $data
        ];

        return json_encode($json_data);
    }
}
