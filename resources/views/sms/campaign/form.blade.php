<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">First Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('first_name', null, ['class' => 'form-control','placeholder'=>'Enter First Name','autocomplete'=>'off','id'=>'first_name','required']) !!}
        </div>
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Last Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::text('last_name', null, ['class' => 'form-control','placeholder'=>'Enter Last Name','autocomplete'=>'off','id'=>'last_name']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Number</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::number('number', null, ['class' => 'form-control','placeholder'=>'Enter Number','autocomplete'=>'off','id'=>'number','required']) !!}
        </div>
    </div>
</div>

<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Birthday</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        <div class="input-group">
            {!! Form::date('birthday', null, ['class' => 'form-control','placeholder'=>'Enter Birthday','autocomplete'=>'off','id'=>'birthday']) !!}
        </div>
    </div>
</div>


{!! Form::hidden('business_id', Auth::user()->business->id) !!}
