<?php

namespace App\Http\Controllers;

use App\Mail\SenderIdActivated;
use App\Models\Business;
use App\Models\ContactGroup;
use App\Models\Invoice;
use App\Models\SenderID;
use App\Traits\InvoiceTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SenderIDController extends Controller
{
    use InvoiceTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('sender-id.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $sender_id = null;
            DB::transaction(function () use ($request) {
                $business = Business::find($request->business_id);
                $sender_id = new SenderID();
                $sender_id->sender_id = $request->sender_id;
                $sender_id->business_id = $business->id;
                $sender_id->charge = $business->sender_id_charge;
                $sender_id->added_by = Auth::user()->id;
                $sender_id->save();

                $freeCount = $business->free_sender_id_count;
                if ($freeCount > 0) {
                    $invoice = $this->generateInvoice($sender_id->business_id, 'App/SenderID', $sender_id->id, $sender_id->charge, 'free');
                    $business->free_sender_id_count = $business->free_sender_id_count - 1;
                    $sender_id->is_free = 1;
                    $business->save();
                } else {
                    $invoice = $this->generateInvoice($sender_id->business_id, 'App/SenderID', $sender_id->id, $sender_id->charge, 'pending');
                }

                $sender_id->invoice_id = $invoice->id;
                $sender_id->save();
            });
            return $this->sendResponse($sender_id, 'Sender ID Requested Successfully');
        } catch (\Exception $exception) {
            return $this->sendError('Something went wrong Please Contact System Administrator');
        }


    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\SenderID $senderID
     * @return \Illuminate\Http\Response
     */
    public function show(SenderID $senderID)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\SenderID $senderID
     * @return \Illuminate\Http\Response
     */
    public function edit(SenderID $senderID)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\SenderID $senderID
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SenderID $senderID)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\SenderID $senderID
     * @return \Illuminate\Http\Response
     */
    public function destroy(SenderID $senderID)
    {

    }

    public function datatable(Request $request)
    {
        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'sender_id', 'status'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = SenderID::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = SenderID::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }
        //  dd($dataset);
        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contact-groups edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contact-groups destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;


        foreach ($dataset as $key => $item) {
            $make_payment =null;
            $status = $this->generateStatusLabel($item->status);
            $invoice_url = route('invoices.show', [$item->invoice_id]);

            $upload_documents = ($item->status == 'pending_documents') ? "<button  onclick=\"uploadDocuments(this)\" data-id='{$item->id}'  class='btn btn-xs  btn-light-warning mr-2'> <i class='fa fa-upload mr-2'></i>Upload Documents</button>" : null;
            $make_payment = ($item->is_free != 1 && ($item->is_paid == 0) && $item->status != 'active') ? "<button onclick=\"paymentSenderId(this)\" data-id='{$item->id}'  data-invoice ='{$item->invoice}' data-user='{$item->user}' class='btn btn-xs  btn-danger mr-2' > <i class='fa fa-dollar-sign'></i>Make Payment</button>" : null;
            $invoice = "<a  target='_blank' href='$invoice_url' class='btn btn-xs btn-light-info mr-2' data-toggle='tooltip' data-placement='top' title='View Invoice'> <i class='fa fa-file-invoice-dollar mr-2'></i>View Invoice</a>";

            $btnActivate = null;

            if ($user->can('sender-id activate')) {
                if ($item->status != 'active') {
                    if ($user->hasRole(['Admin'])) {
                        if (($item->is_paid || $item->is_free) && $item->status == 'approving') {
                            $btnActivate = "<button  onclick=\"activateSenderID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Activate</button>";
                        }
                    } else {
                        $btnActivate = "<button  onclick=\"activateSenderID($item->id)\" data-id='{$item->id}'  class='btn btn-xs  btn-info mr-2'> <i class='fa fa-check mr-2'></i>Activate</button>";
                    }
                }

            }


            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = [
                    $item->id,
                    optional($item->business)->name,
                    optional($item->user)->name,
                    $item->sender_id,
                    $status,
                    $upload_documents . $make_payment  . $btnActivate.$invoice
                ];
            } else {
                $data[$i] = [
                    $item->id,
                    $item->sender_id,
                    $status,
                    $upload_documents . $make_payment . $invoice
                ];
            }


            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function generateStatusLabel($status)
    {
        $label = null;
        $text = ucfirst(str_replace('_', ' ', $status));
        switch ($status) {
            case 'requested':
                $label = "<label class='label font-weight-bold label-lg  label-warning label-inline'>$text</label>";
                break;
            case 'pending_documents':
            case 'pending_payment':
                $label = "<label class='label font-weight-bold label-lg  label-light-danger label-inline'>$text</label>";
                break;
            case 'approving':
                $label = "<label class='label font-weight-bold label-lg  label-primary label-inline'>$text</label>";
                break;
            case 'active':
                $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>$text</label>";
                break;
            case 'disabled':
                $label = "<label class='label font-weight-bold label-lg  label-danger label-inline'>$text</label>";
                break;
            case 'paid':
                $label = "<label class='label font-weight-bold label-lg  label-success label-inline'>$text</label>";
                break;
        }
        return $label;
    }


    public function uploadDocument(Request $request, SenderID $senderID)
    {
        $fileName = $senderID->sender_id . '_noc.pdf';
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = storage_path() . '/sender_id/noc/';
            $file->move($destinationPath, $fileName);

            if ($senderID->is_free || $senderID->is_paid) {
                $senderID->status = 'approving';
            } else {
                $senderID->status = 'pending_payment';
            }
            $senderID->noc_letter_path = '/sender_id/noc/' . $fileName;
            $senderID->noc_letter_uploaded_at = Carbon::now()->format('Y-m-d H:i:s');
            $senderID->save();
            return $this->sendResponse($fileName, 'Document Uploaded Successfully');

        } else {
            return $this->sendError('Document Uploaded Failed');
        }
    }

    public function uploadReceipt(Request $request, SenderID $senderID)
    {

        $status = (!empty($senderID->noc_letter_path)) ? 'approving' : 'pending_documents';

        $senderID->update([
            'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'payment_method' => "bank_transfer",
            'status' => $status,
        ]);

        $invoice = Invoice::where('id', $senderID->invoice_id)->first();
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $fileName = $file->getClientOriginalName();
            $destinationPath = storage_path() . '/sender_id/receipt/';
            $file->move($destinationPath, $fileName);

            $invoice->update([
                'receipt_path' => '/sender_id/receipt/' . $fileName,
                'paid_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'paid_by' => Auth::user()->id,
                'payment_method' => "bank_transfer",
                'status' => "approving",
            ]);

            return $this->sendResponse($fileName, 'Document Uploaded Successfully');

        } else {
            return $this->sendError('Document Uploaded Failed');
        }
    }


    public function activate(Request $request, SenderID $senderID)
    {
        if (Auth::user() && Auth::user()->can('sender-id activate')) {
            $senderID->status = 'active';
            $senderID->verified_at = Carbon::now()->format('Y-m-d H:i:s');
            $senderID->verified_by = Auth::id();
            $senderID->save();

            $invoice = Invoice::where('id',$senderID->invoice_id)->first();
            $invoice->status = 'paid';
            $invoice->approved_by = Auth::id();
            $invoice->approved_at = Carbon::now()->format('Y-m-d H:i:s');
            $invoice->save();

            Mail::to($senderID->business->owner)->queue(new SenderIdActivated($senderID));
            return $this->sendResponse($senderID, 'Sender ID Activated Successfully!');
        } else {
            return $this->sendError('Permission Denied');
        }
    }

}
