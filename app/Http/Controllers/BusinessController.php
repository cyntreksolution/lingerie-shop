<?php

namespace App\Http\Controllers;

use App\Models\Business;
use App\Models\SenderID;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Str;

class BusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('business.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function show(Business $business)
    {
        return view('business.profile', compact('business'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function edit(Business $business)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Business $business)
    {
        if (!empty($request->balance)) {
            $business->balance = $request->balance;
        }

        if (!empty($request->affiliate_balance)) {
            $business->affiliate_balance = $request->affiliate_balance;
        }

        if (!empty($request->sms_charge)) {
            $business->sms_charge = $request->sms_charge;
        }

        if (!empty($request->sender_id_charge)) {
            $business->sender_id_charge = $request->sender_id_charge;
        }

        if (!empty($request->free_sender_id_count)) {
            $business->free_sender_id_count = $request->free_sender_id_count;
        }

        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/business_logo/';
            $fileName = $business->id . '.' . $file->getClientOriginalExtension();;
            $file->move($destinationPath, $fileName);
            $business->logo = '/business_logo/' . $fileName;
        }

        if (!empty($request->name)) {
            $business->name = $request->name;
        }

        if (!empty($request->address)) {
            $business->address = $request->address;
        }

        if (!empty($request->active_api)) {
            $api_status = ($request->api_enabled == 'on') ? 1 : 0;
            $business->api_enabled = $api_status;

            if ($api_status == 1) {

                if (empty($business->business_id)) {
                    $business->business_id = rand(100001, 999999);
                }

                if (empty($business->business_secret)) {
                    $business->business_secret = Str::random(32);
                }
            }
        }

        if (!empty($request->active_otp)) {
            $otp_status = ($request->otp_enabled == 'on') ? 1 : 0;
            $business->otp_enabled = $otp_status;
        }

        $business->save();

        if (!empty($request->balance)) {
            return $this->sendResponse($business, 'Business Updated Successfully');
        }

        return redirect(route('business.show', $business->id));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Business $business
     * @return \Illuminate\Http\Response
     */
    public function destroy(Business $business)
    {
        //
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'name', 'owner', 'balance', 'sms_charge', 'sender_id_charge', 'free_sender_id_count'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Business::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Business::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }
        //  dd($dataset);
        $data = [];
        $i = 0;

        $user = Auth::user();
        $can_edit = ($user->can('business edit')) ? 1 : 0;
        $can_delete = ($user->can('business destroy')) ? 1 : 0;


        foreach ($dataset as $key => $item) {
            $edit_btn = null;
            $delete_btn = null;

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' onclick=\"edit(this)\" data-id='{$item->id}' data-name='{$item->name}'  data-affiliate_balance='{$item->affiliate_balance}'   data-balance='{$item->balance}'  data-sms_charge='{$item->sms_charge}'  data-sender_id_charge='{$item->sender_id_charge}'   data-free_sender_id_count='{$item->free_sender_id_count}' ><i class='fa fa-pencil-alt '  ></i></button>";
            }
            if ($can_delete) {
                $url = "'business/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }


            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = [
                    $item->id,
                    $item->name,
                    optional($item->owner)->name,
                    number_format($item->balance, 2),
                    number_format($item->affiliate_balance, 2),
                    number_format($item->sms_charge, 2),
                    number_format($item->sender_id_charge, 2),
                    $item->free_sender_id_count,
                    $edit_btn . $delete_btn
                ];
            } else {
                $data[$i] = [
                    $item->name,
                    $item->balance,
                    $item->sms_charge,
                    $item->sender_id_charge,
                ];
            }


            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }


    public function affiliateStatusChange(Request $request, Business $business)
    {


        if (!empty($request->active_affiliate)) {
            $api_status = ($request->affiliate_enabled == 'on') ? 1 : 0;
            $business->affiliate_enabled = $api_status;

            if ($api_status == 1) {
                $business->affiliate_key = Str::random(32);
                $business->affiliate_url = route('register')."?affiliateString=". $business->affiliate_key;
                $business->account_holder_name = $request->account_holder_name;
                $business->branch = $request->branch;
                $business->account_number = $request->account_number;
            }

            $business->save();
        }

        return redirect(route('business.show', $business->id));
    }
}
