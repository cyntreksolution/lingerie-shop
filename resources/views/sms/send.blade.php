@extends('layouts.master')
@section('title','Send Instant SMS')
@section('content')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <div class="card card-custom gutter-b mt-3" style="margin-top: 50px !important;">
                <div class="card-header flex-wrap py-3">
                    <div class="card-title">
                        <h3 class="card-label">Send Instant SMS
                            <span
                                class="d-block text-muted pt-2 font-size-sm"> Send SMS</span>
                        </h3>
                    </div>
                    <div class="card-toolbar">


                    </div>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'sms.store', 'method' => 'post','id'=>'createForm']) !!}
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-form-label text-right col-lg-3 col-sm-12">Sender ID</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    {!! Form::select('sender_id', $sender_ids , null , ['id'=>'sender_id','class' => 'w-100 form-control selectpicker','data-live-search'=>"true",'placeholder'=>'Select Sender ID','required']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label text-right col-lg-3 col-sm-12">Number </label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    {!! Form::number('number', null, ['id'=>'numbers','class' => 'numbers form-control js-number','placeholder'=>'Enter Numbers','minlength'=>9,'maxlength'=>12 ]) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label text-right col-lg-3 col-sm-12">Contact</label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    {!! Form::select('contact', $contacts , null , ['id'=>'contact','class' => 'w-100 form-control selectpicker js-number','data-live-search'=>"true",'placeholder'=>'Select a Contact']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label text-right col-lg-3 col-sm-12">Message </label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    {!! Form::textarea('message', null, ['id'=>'message','class' => 'form-control','placeholder'=>'Enter Message','required']) !!}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-form-label text-right col-lg-3 col-sm-12"> </label>
                                <div class="col-lg-9 col-md-9 col-sm-12">
                                    <button type="button" onclick="sendMsg()" ; class="btn btn-block btn-success" data-toggle="tooltip" data-placement="top" title="Send"><i
                                            class="fa fa-mail-bulk"></i> Send
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6" id="sms-counter">
                            <!--begin::Nav Panel Widget 3-->
                            <div class="card card-custom card-stretch gutter-b">
                                <!--begin::Body-->
                                <div class="card-body">
                                    <!--begin::Wrapper-->
                                    <div class="d-flex justify-content-between flex-column h-100">
                                        <!--begin::Container-->
                                        <div class="h-100">

                                            <div class="pt-1">
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-45 symbol-light mr-4">
																	<span class="symbol-label">
																		<span class="svg-icon svg-icon-2x svg-icon-dark-50">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Media/Equalizer.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24"></rect>
																					<rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
																					<rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
																					<rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
																					<rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
																				</g>
																			</svg>
                                                                            <!--end::Svg Icon-->
																		</span>
																	</span>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Characters</a>
{{--                                                        <span class="text-muted font-weight-bold">Good Fellas</span>--}}
                                                    </div>
                                                    <!--end::Text-->
                                                    <!--begin::label-->
                                                    <span class="font-weight-bolder label label-xl label-inline px-3 py-5 min-w-45px length" >0</span>
                                                    <!--end::label-->
                                                </div>
                                                <hr>
                                                <!--end::Item-->
                                                <!--end::Item-->
                                                <!--begin::Item-->
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-45 symbol-light mr-4">
																	<span class="symbol-label">
																		<span class="svg-icon svg-icon-2x svg-icon-dark-50">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Communication/Group.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<polygon points="0 0 24 0 24 24 0 24"></polygon>
																					<path d="M18,14 C16.3431458,14 15,12.6568542 15,11 C15,9.34314575 16.3431458,8 18,8 C19.6568542,8 21,9.34314575 21,11 C21,12.6568542 19.6568542,14 18,14 Z M9,11 C6.790861,11 5,9.209139 5,7 C5,4.790861 6.790861,3 9,3 C11.209139,3 13,4.790861 13,7 C13,9.209139 11.209139,11 9,11 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
																					<path d="M17.6011961,15.0006174 C21.0077043,15.0378534 23.7891749,16.7601418 23.9984937,20.4 C24.0069246,20.5466056 23.9984937,21 23.4559499,21 L19.6,21 C19.6,18.7490654 18.8562935,16.6718327 17.6011961,15.0006174 Z M0.00065168429,20.1992055 C0.388258525,15.4265159 4.26191235,13 8.98334134,13 C13.7712164,13 17.7048837,15.2931929 17.9979143,20.2 C18.0095879,20.3954741 17.9979143,21 17.2466999,21 C13.541124,21 8.03472472,21 0.727502227,21 C0.476712155,21 -0.0204617505,20.45918 0.00065168429,20.1992055 Z" fill="#000000" fill-rule="nonzero"></path>
																				</g>
																			</svg>
                                                                            <!--end::Svg Icon-->
																		</span>
																	</span>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Pages</a>
{{--                                                        <span class="text-muted font-weight-bold">Successful Fellas</span>--}}
                                                    </div>
                                                    <!--end::Text-->
                                                    <!--begin::label-->
                                                    <span class="font-weight-bolder label label-xl label-inline px-3 py-5 min-w-45px messages">0</span>
                                                    <!--end::label-->
                                                </div>
                                                <hr>
                                                <div class="d-flex align-items-center">
                                                    <!--begin::Symbol-->
                                                    <div class="symbol symbol-45 symbol-light mr-4">
																<span class="symbol-label">
																		<span class="svg-icon svg-icon-2x svg-icon-dark-50">
																			<!--begin::Svg Icon | path:/metronic/theme/html/demo2/dist/assets/media/svg/icons/Home/Globe.svg-->
																			<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
																				<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
																					<rect x="0" y="0" width="24" height="24"></rect>
																					<path d="M13,18.9450712 L13,20 L14,20 C15.1045695,20 16,20.8954305 16,22 L8,22 C8,20.8954305 8.8954305,20 10,20 L11,20 L11,18.9448245 C9.02872877,18.7261967 7.20827378,17.866394 5.79372555,16.5182701 L4.73856106,17.6741866 C4.36621808,18.0820826 3.73370941,18.110904 3.32581341,17.7385611 C2.9179174,17.3662181 2.88909597,16.7337094 3.26143894,16.3258134 L5.04940685,14.367122 C5.46150313,13.9156769 6.17860937,13.9363085 6.56406875,14.4106998 C7.88623094,16.037907 9.86320756,17 12,17 C15.8659932,17 19,13.8659932 19,10 C19,7.73468744 17.9175842,5.65198725 16.1214335,4.34123851 C15.6753081,4.01567657 15.5775721,3.39010038 15.903134,2.94397499 C16.228696,2.49784959 16.8542722,2.4001136 17.3003976,2.72567554 C19.6071362,4.40902808 21,7.08906798 21,10 C21,14.6325537 17.4999505,18.4476269 13,18.9450712 Z" fill="#000000" fill-rule="nonzero"></path>
																					<circle fill="#000000" opacity="0.3" cx="12" cy="10" r="6"></circle>
																				</g>
																			</svg>
                                                                            <!--end::Svg Icon-->
																		</span>
																	</span>
                                                    </div>
                                                    <!--end::Symbol-->
                                                    <!--begin::Text-->
                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder">Remain Characters</a>
{{--                                                        <span class="text-muted font-weight-bold">Successful Fellas</span>--}}
                                                    </div>
                                                    <!--end::Text-->
                                                    <!--begin::label-->
                                                    <span class="font-weight-bolder label label-xl label-inline px-3 py-5 min-w-45px remaining">160</span>
                                                    <!--end::label-->
                                                </div>

                                                <hr>
                                                <div class="d-flex align-items-center mt-10">

                                                    <div class="d-flex flex-column flex-grow-1">
                                                        <a href="#" class="text-dark-75 text-hover-primary mb-1 font-size-lg font-weight-bolder"></a>
                                                        <span class="text-muted font-weight-bold">We will remove all Invalid characters and Duplicate numbers from your recipients list before sending your message. And the Above estimated price can be changed.</span>
                                                    </div>
                                                </div>
                                                <!--end::Item-->
                                            </div>
                                            <!--end::Body-->
                                        </div>
                                    </div>
                                    <!--end::Wrapper-->
                                </div>
                                <!--end::Body-->
                            </div>
                            <!--end::Nav Panel Widget 3-->
                        </div>
                    </div>
                    {!! Form::close() !!}


                </div>
            </div>

        </div>

    </div>
@endsection

@push('js')
    <script src="{{asset('js/tagify.js')}}"></script>
    <script src="{{asset('js/sms_counter.min.js')}}"></script>

    <script>
        $('#message').countSms('#sms-counter');
    </script>
    <script>

        function validateFormNow(form_id) {
            return  fv = FormValidation.formValidation(
                document.getElementById(form_id),
                {
                    fields: {
                        message: {
                            validators: {
                                notEmpty: {
                                    message: 'Message is required'
                                },
                            }
                        },
                        sender_id: {
                            validators: {
                                notEmpty: {
                                    message: 'Sender ID is required'
                                },
                            }
                        },
                        number: {
                            validators: {
                                stringLength: {
                                    min:9,
                                    max: 11,
                                    message: 'number should  equal or less than 11 digits and more than or equal to 9 '
                                },
                            }
                        }
                    },
                    plugins: {
                        declarative: new FormValidation.plugins.Declarative({
                            html5Input: true,
                        }),
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap(),
                    }
                }
            );
        }

        function sendMsg() {
            form_id = '#createForm';
            let url = $(form_id).attr('action');
            let method = $(form_id).attr('method');
            validateFormNow('createForm').validate().then(function (status) {
                if (status == "Valid") {
                    $(form_id).ajaxSubmit(
                        {
                            clearForm: true,
                            url: url,
                            type: method,
                            success: function (result) {
                                if (result.success) {
                                    Notifications.showSuccessMsg(result.message);
                                } else {
                                    Notifications.showErrorMsg(result.message);
                                }

                                $("#sender_id").val("");
                                $("#contact").val("");
                                $('sender_id').val('');
                            },
                            error: function (XMLHttpRequest, textStatus, errorThrown) {
                                Notifications.showErrorMsg(errorThrown);
                                $('sender_id').val('');
                            }
                        }
                    );


                }

            })
        }

    </script>
@endpush
