<?php

namespace App\Http\Controllers;

use App\Imports\ContactsImport;
use App\Models\Contact;
use App\Models\ContactGroup;
use App\Models\ContactGroupContact;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = ContactGroup::pluck('name','id');
        return view('contacts.index',compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $contact = Contact::create($request->all());

        if (!empty($request->contact_group)) {
            foreach (array_filter($request->contact_group) as $group) {
                $n = new ContactGroupContact();
                $n->contact_group_id = $group;
                $n->contact_id = $contact->id;
                $n->save();
            }
        }
        return $this->sendResponse($contact, 'Contact Created Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        $exists = Contact::whereNumber($request->number)->where('id','<>',$contact->id)->exists();
        if ($exists) {
            return $this->sendError( 'Duplicate Number');
        }
        $contact->first_name = $request->first_name;
        $contact->last_name = $request->last_name;
        $contact->number = $request->number;
        $contact->birthday = $request->birthday;
        $contact->save();

        if (!empty($request->contact_group)) {
            foreach (array_filter($request->contact_group) as $group) {
                $x = ContactGroupContact::firstOrCreate(['contact_group_id'=>$group,'contact_id'=>$contact->id]);
//                $n = new ContactGroupContact();
//                $n->contact_group_id = $group;
//                $n->contact_id = $contact->id;
//                $n->save();
            }
        }

        return $this->sendResponse($contact, 'Contact Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Contact $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $contact->delete();
        return $this->sendResponse($contact, 'Contact Deleted Successfully');
    }

    public function datatable(Request $request)
    {

        $order_by = $request->order;
        $search = $request->search['value'];
        $start = $request->start;
        $length = $request->length;
        $order_by_str = $order_by[0]['dir'];

        $columns = ['id', 'first_name', 'last_name', 'number', 'birthday'];
        $order_column = $columns[$order_by[0]['column']];
        $dataset = Contact::tableData($order_column, $order_by_str, $start, $length);

        if (is_null($search) || empty($search)) {
            $dataset = $dataset->get();
            $channels_count = Contact::all()->count();
        } else {
            $dataset = $dataset->searchData($search)->get();
            $channels_count = $dataset->count();
        }

        $data = [];
        $i = 0;
        $edit_btn = null;
        $delete_btn = null;
        $user = Auth::user();
        $can_edit = ($user->hasPermissionTo('contacts edit')) ? 1 : 0;
        $can_delete = ($user->hasPermissionTo('contacts destroy')) ? 1 : 0;
        $can_edit = $can_delete = 1;
        foreach ($dataset as $key => $item) {

            if ($can_edit) {
                $edit_btn = "<button  class='btn btn-xs btn-icon btn-light-info mr-2' data-toggle='tooltip' data-placement='top' title='Edit'  onclick=\"edit(this)\" data-id='{$item->id}' data-first_name='{$item->first_name}' data-last_name='{$item->last_name}' data-number='{$item->number}'' data-birthday='{$item->birthday}'><i class='fa fa-pencil-alt '  ></i></button>";
            }
            if ($can_delete) {
                $url = "'contacts/" . $item->id . "'";
                $delete_btn = "<button class='btn btn-xs btn-icon btn-light-danger mr-2' data-toggle='tooltip' data-placement='top' title='Delete' onclick=\"FormOptions.deleteRecord(" . $item->id . ",$url,'datatable')\"> <i class='fa fa-trash'></i></button>";
            }



            if ($user->hasRole((['Super Admin', 'Admin']))) {
                $data[$i] = array(
                    $item->id,
                    $item->business->name,
                    $item->first_name,
                    $item->last_name,
                    $item->number,
                    $item->birthday,
                    $item->groups->pluck('name'),
                    $edit_btn . $delete_btn
                );
            }else{
                $data[$i] = array(
                    $item->id,
                    $item->first_name,
                    $item->last_name,
                    $item->number,
                    $item->birthday,
                    $item->groups->pluck('name'),
                    $edit_btn . $delete_btn
                );
            }

            $i++;
        }


        if ($channels_count == 0) {
            $data = [];
        }

        $json_data = [
            "draw" => intval($_REQUEST['draw']),
            "recordsTotal" => intval($channels_count),
            "recordsFiltered" => intval($channels_count),
            "data" => $data
        ];

        return json_encode($json_data);
    }

    public function contactsImport(Request $request)
    {
        $business_id = Auth::user()->business_id;
        if ($request->hasFile('file')) {
            $file = $request->file('file');
            $destinationPath = public_path() . '/tmp_numbers/';
            $fileName = 'temp_numberList' . uniqid() . '.txt';
            $file->move($destinationPath, $fileName);
            $pdo = DB::connection()->getPdo();

            $pdo->beginTransaction();
            $path = str_replace('\\', '/', $destinationPath);


            $lines = file("$path$fileName");
            $line = $lines[0];
            switch (TRUE) {
                case substr($line, -2) === "\r\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id  ";
                    $pdo->exec($query);
                    break;
                case substr($line, -1) === "\n" :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '
' IGNORE 1 LINES (first_name,last_name,number,birthday) SET business_id =  $business_id ";
                    $pdo->exec($query);
                    break;
                default :
                    $query = "LOAD DATA LOCAL INFILE '$path$fileName' IGNORE INTO TABLE contacts FIELDS OPTIONALLY ENCLOSED BY '\"' TERMINATED BY ',' LINES TERMINATED BY '\r\n' IGNORE 1 LINES(first_name,last_name,number,birthday) SET business_id =  $business_id ";
                    $pdo->exec($query);
                    break;

            }


            $pdo->commit();

            File::delete($destinationPath . $fileName);

        }

        return $this->sendResponse($business_id, 'Contact Group Created Successfully');
//        Excel::import(new ContactsImport($business_id), request()->file('filename'));
    }

    public function validateNumber(Request $request)
    {
        $biz = $request->biz;
        $number = $request->number;
        $exists = Contact::whereBusinessId($biz)->whereNumber($number)->exists();
        return ($exists) ? 0 : 1;
    }
}
