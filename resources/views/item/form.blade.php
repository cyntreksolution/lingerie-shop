<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Category</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::select('category',$categories, null, ['class' => 'form-control','placeholder'=>'Select Category','autocomplete'=>'off','id'=>'category_id','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Brand</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::select('brand',$brands, null, ['class' => 'form-control','placeholder'=>'Select Brand','autocomplete'=>'off','id'=>'brand_id','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Name</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'name','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Price</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::text('price', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'price','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Compared Price</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::text('compared_price', null, ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'compare_price','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Featured Image</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::file('image', ['class' => 'form-control','placeholder'=>'Enter Name','autocomplete'=>'off','id'=>'image','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Description</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::textarea('short_description', null, ['id'=>'short_description','class' => 'form-control','placeholder'=>'Enter Description','required']) !!}
    </div>
</div>
<div class="form-group row">
    <label class="col-form-label text-right col-lg-3 col-sm-12">Description</label>
    <div class="col-lg-9 col-md-9 col-sm-12">
        {!! Form::textarea('long_description', null, ['id'=>'long_description','class' => 'form-control','placeholder'=>'Enter Description','required']) !!}
    </div>
</div>



