<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Home</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="apple-touch-icon" href="images/icon.png">


    <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i,900" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,600,600i,700,700i,800"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Satisfy" rel="stylesheet">


    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/plugins.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">


    <link rel="stylesheet" href="{{asset('css/custom.css')}}">

    @stack('styles')

    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
</head>

<body>
<!--[if lte IE 9]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade
    your browser</a> to improve your experience and security.</p>
<![endif]-->


<div class="wrapper" id="wrapper">

    <header id="wn__header" class="header__area header--fullwidth space-between header--one sticky__header">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 d-none d-lg-block">
                    <div class="menubar">
                        <a class="open_sidebar block__active">menu</a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-6 col-6">
                    <div class="logo text-left text-lg-center">
                        <a href="/">
                            <img src="{{asset('images/logo/liyona.png')}}" style="height: 65px" alt="logo images">
                        </a>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-6 col-6">
                    <ul class="header__sidebar__right d-flex justify-content-end align-items-center">

                        <li class="shopcart">
                            <a class="cartbox_active"  disabled="" href="#">
                            </a>

                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </header>

@include('layouts.menu')


@yield('content')


    <footer id="wn__footer" class="footer__area footer--four">
        <div class="footer-static-top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer__widget footer__menu">
                            <div class="ft__logo">
                                <a href="/">
                                    <img style="height: 100px" src="{{asset('images/logo/liyona.png')}}" alt="logo">
                                </a>
                            </div>
                            <div class="footer__content">
                                <ul class="mainmenu d-flex justify-content-center">
                                    <li><a href="/">home</a></li>
                                    <li><a href="#">Customer Service</a></li>
                                    <li><a href="#">policies</a></li>
                                    <li><a href="#">about us</a></li>
                                    <li><a href="#">contact</a></li>
                                </ul>
                                <ul class="social__net mt--30 social__net--2 d-flex justify-content-center">
                                    <li><a href="#"><i class="zmdi zmdi-rss"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-linkedin"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-vimeo"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-tumblr"></i></a></li>
                                    <li><a href="#"><i class="zmdi zmdi-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright__wrapper bg--white">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="copyright">
                            <div class="copy__right__inner text-center">
                                <p> Powered By <i class="mdi mdi-heart text-danger"></i><a
                                        href="https://cyntrek.com/">Cyntrek Solutions .</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- //Footer Area -->

    <!-- QUICKVIEW PRODUCT -->
    <div id="quickview-wrapper">
        <!-- Modal -->
{{--        <div class="modal fade" id="productmodal" tabindex="-1" role="dialog">--}}
{{--            <div class="modal-dialog modal__container" role="document">--}}
{{--                <div class="modal-content">--}}
{{--                    <div class="modal-header modal__header">--}}
{{--                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span--}}
{{--                                aria-hidden="true">&times;</span></button>--}}
{{--                    </div>--}}
{{--                    <div class="modal-body">--}}
{{--                        <div class="modal-product">--}}
{{--                            <!-- Start product images -->--}}
{{--                            <div class="product-images">--}}
{{--                                <div class="main-image images">--}}
{{--                                    <img alt="big images" src="images/product/big-img/1.jpg">--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <!-- end product images -->--}}
{{--                            <div class="product-info">--}}
{{--                                <h1>Simple Fabric Bags</h1>--}}
{{--                                <div class="rating__and__review">--}}
{{--                                    <ul class="rating">--}}
{{--                                        <li><span class="ti-star"></span></li>--}}
{{--                                        <li><span class="ti-star"></span></li>--}}
{{--                                        <li><span class="ti-star"></span></li>--}}
{{--                                        <li><span class="ti-star"></span></li>--}}
{{--                                        <li><span class="ti-star"></span></li>--}}
{{--                                    </ul>--}}
{{--                                    <div class="review">--}}
{{--                                        <a href="#">4 customer reviews</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="price-box-3">--}}
{{--                                    <div class="s-price-box">--}}
{{--                                        <span class="new-price">$17.20</span>--}}
{{--                                        <span class="old-price">$45.00</span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="quick-desc">--}}
{{--                                    Designed for simplicity and made from high quality materials. Its sleek geometry and--}}
{{--                                    material combinations creates a modern look.--}}
{{--                                </div>--}}
{{--                                <div class="select__color">--}}
{{--                                    <h2>Select color</h2>--}}
{{--                                    <ul class="color__list">--}}
{{--                                        <li class="red"><a title="Red" href="#">Red</a></li>--}}
{{--                                        <li class="gold"><a title="Gold" href="#">Gold</a></li>--}}
{{--                                        <li class="orange"><a title="Orange" href="#">Orange</a></li>--}}
{{--                                        <li class="orange"><a title="Orange" href="#">Orange</a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                                <div class="select__size">--}}
{{--                                    <h2>Select size</h2>--}}
{{--                                    <ul class="color__list">--}}
{{--                                        <li class="l__size"><a title="L" href="#">L</a></li>--}}
{{--                                        <li class="m__size"><a title="M" href="#">M</a></li>--}}
{{--                                        <li class="s__size"><a title="S" href="#">S</a></li>--}}
{{--                                        <li class="xl__size"><a title="XL" href="#">XL</a></li>--}}
{{--                                        <li class="xxl__size"><a title="XXL" href="#">XXL</a></li>--}}
{{--                                    </ul>--}}
{{--                                </div>--}}
{{--                                <div class="social-sharing">--}}
{{--                                    <div class="widget widget_socialsharing_widget">--}}
{{--                                        <h3 class="widget-title-modal">Share this product</h3>--}}
{{--                                        <ul class="social__net social__net--2 d-flex justify-content-start">--}}
{{--                                            <li class="facebook"><a target="_blank" href="#" class="rss social-icon"><i--}}
{{--                                                        class="zmdi zmdi-rss"></i></a></li>--}}
{{--                                            <li class="linkedin"><a target="_blank" href="#"--}}
{{--                                                                    class="linkedin social-icon"><i--}}
{{--                                                        class="zmdi zmdi-linkedin"></i></a></li>--}}
{{--                                            <li class="pinterest"><a target="_blank" href="#"--}}
{{--                                                                     class="pinterest social-icon"><i--}}
{{--                                                        class="zmdi zmdi-pinterest"></i></a></li>--}}
{{--                                            <li class="tumblr"><a target="_blank" href="#" class="tumblr social-icon"><i--}}
{{--                                                        class="zmdi zmdi-tumblr"></i></a></li>--}}
{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="addtocart-btn">--}}
{{--                                    <a href="#">Add to cart</a>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
    </div>
    <!-- END QUICKVIEW PRODUCT -->

</div>
<!-- //Main wrapper -->

<!-- JS Files -->
<script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
<script src="{{asset('js/popper.min.js')}}"></script>
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/plugins.js')}}"></script>
<script src="{{asset('js/active.js')}}"></script>
@stack('scripts')

</body>

</html>
